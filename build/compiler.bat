@REM Compilacion y link con SFML 2.5.1.
@cls
@echo     _/      _/    _/_/_/    _/_/_/        _/                  _/    _/_/_/  _/_/_/_/
@echo    _/_/  _/_/  _/        _/            _/  _/                _/  _/        _/
@echo   _/  _/  _/  _/  _/_/  _/              _/_/  _/            _/  _/  _/_/  _/_/_/
@echo  _/      _/  _/    _/  _/            _/    _/        _/    _/  _/    _/  _/
@echo _/      _/    _/_/_/    _/_/_/        _/_/  _/        _/_/      _/_/_/  _/
@echo -----------------------------------------------------------------------------------
@echo  Welcome to the Hardest Game compiler
@echo  How do you want to compile the program?
@echo  [1] Debug
@echo  [2] Release
@echo  [3] Exit
@echo -----------------------------------------------------------------------------------
@echo off

:accept_input
  @set input=3
  @set /p input="Enter an option: "
  IF "%input%"=="1" GOTO:compile_debug
  IF "%input%"=="2" GOTO:compile_release
  IF "%input%"=="3" GOTO:not_compiled
  GOTO:accept_input

:compile_debug

cl /nologo /Zi /GR- /EHs /MDd /W3 -DSFML_STATIC /c ..\src\*.cc ..\src\*.c  ..\src\*.cpp -I ..\include -I ..\deps\SFML-2.5.1-32\include -I ..\deps\imgui
cl /nologo /Zi /GR- /EHs /MDd /W3 /Fe:..\bin\hardest_d.exe *.obj ..\deps\SFML-2.5.1-32\lib\sfml-graphics-s-d.lib ..\deps\SFML-2.5.1-32\lib\sfml-window-s-d.lib ..\deps\SFML-2.5.1-32\lib\sfml-system-s-d.lib ..\deps\SFML-2.5.1-32\lib\freetype.lib opengl32.lib winmm.lib gdi32.lib user32.lib Advapi32.lib

del *.obj *.ilk *.pdb ..\bin\*.obj  ..\bin\*.ilk  ..\bin\*.pdb

@echo -----------------------------------------------------------------------------------
@echo  Correctly compiled in Debug.
@echo -----------------------------------------------------------------------------------
GOTO:EOF

:compile_release

cl /nologo /Zi /GR- /EHs /MD /W3 -DSFML_STATIC /c ..\src\*.cc ..\src\*.c  ..\src\*.cpp -I ..\include -I ..\deps\SFML-2.5.1-32\include -I ..\deps\imgui
cl /nologo /Zi /GR- /EHs /MD /W3 /Fe:..\bin\hardest_r.exe *.obj ..\deps\SFML-2.5.1-32\lib\sfml-graphics-s.lib ..\deps\SFML-2.5.1-32\lib\sfml-window-s.lib ..\deps\SFML-2.5.1-32\lib\sfml-system-s.lib ..\deps\SFML-2.5.1-32\lib\freetype.lib opengl32.lib winmm.lib gdi32.lib user32.lib Advapi32.lib

del *.obj *.ilk *.pdb ..\bin\*.obj  ..\bin\*.ilk  ..\bin\*.pdb

@echo -----------------------------------------------------------------------------------
@echo  Correctly compiled in Release.
@echo -----------------------------------------------------------------------------------
GOTO:EOF

:not_compiled
@echo -----------------------------------------------------------------------------------
@echo  Not compiled.
@echo -----------------------------------------------------------------------------------
