var searchData=
[
  ['scene_264',['Scene',['../class_scene.html#ad10176d75a9cc0da56626f682d083507',1,'Scene::Scene()'],['../class_scene.html#a57c8be0fe08a98890f2230fbbd122f39',1,'Scene::Scene(int newID)']]],
  ['selectscene_265',['SelectScene',['../class_select_scene.html#ac26f2a772f48475e1088e354606b862e',1,'SelectScene::SelectScene()'],['../class_select_scene.html#a22218ec2ae9b6cec991ec4cf010c40c8',1,'SelectScene::SelectScene(int newId)'],['../class_select_scene.html#abc5317fbf9dca1ae62d309f3ce88832a',1,'SelectScene::selectScene()']]],
  ['setfillcolor_266',['setFillColor',['../class_label.html#a927a925e2547621f47a5da014eabd70c',1,'Label::setFillColor()'],['../class_poly.html#a29c4eda91debfff0c05219ef4bde3fd3',1,'Poly::setFillColor()']]],
  ['setfontsize_267',['setFontSize',['../class_label.html#a6830728ad1831e7de6116c2e7e3d8deb',1,'Label']]],
  ['setletterspacing_268',['setLetterSpacing',['../class_label.html#ac7b6165659d03801eaceef1c2f5453e4',1,'Label']]],
  ['setlinespacing_269',['setLineSpacing',['../class_label.html#a02cae18d32a68736b2000197fc66a362',1,'Label']]],
  ['setoutlinecolor_270',['setOutlineColor',['../class_label.html#ab19a5abcff94f3c3074284282ae9bb84',1,'Label::setOutlineColor()'],['../class_poly.html#a31dea59ec229c05e6bd80f09494f580b',1,'Poly::setOutlineColor()']]],
  ['setoutlinethickness_271',['setOutlineThickness',['../class_poly.html#ab9a63adf4bca60bbc0e0cd871fb69572',1,'Poly']]],
  ['setpointcount_272',['setPointCount',['../class_poly.html#a7028151221451406f4cc3447cd6f6084',1,'Poly']]],
  ['setposition_273',['setPosition',['../class_entity.html#a429176a94e63543d19a5f06ade44efd1',1,'Entity::setPosition()'],['../class_poly.html#ac151a52c04b1e9dc9259c1499381c791',1,'Poly::setPosition()'],['../class_sprite.html#af36ea3cc25a30d9f0b07bfbc05de2b1e',1,'Sprite::setPosition()']]],
  ['setradius_274',['setRadius',['../class_poly.html#aa80549eb5e4815c0483fc0fd37056d93',1,'Poly']]],
  ['setrotation_275',['setRotation',['../class_poly.html#a23f5e89e8731ce8075902e1f4182d305',1,'Poly::setRotation()'],['../class_sprite.html#ac760ebce06dec17a53e2faab325aa9c2',1,'Sprite::setRotation()']]],
  ['setscale_276',['setScale',['../class_poly.html#ae878b27a442003063d22deaeb60195a2',1,'Poly::setScale()'],['../class_sprite.html#a7bb559576210f5cecbb3ed2d8557fc58',1,'Sprite::setScale()']]],
  ['setstring_277',['setString',['../class_label.html#ae01469dd6cc57aa4a8682351db1bdd9c',1,'Label']]],
  ['setthickness_278',['setThickness',['../class_label.html#a6ed200458858bb0c2508b97017c5312e',1,'Label']]],
  ['sprite_279',['Sprite',['../class_sprite.html#a12cba3ac1868418add3c4d95ce87e615',1,'Sprite::Sprite()'],['../class_sprite.html#aa5cb9fac0cfa5d81dc429e75137179d0',1,'Sprite::Sprite(const Sprite &amp;o)'],['../class_sprite.html#a1e5255cb59b3970d11f8655b963684df',1,'Sprite::Sprite(EntityTag newTag, uint8_t newEnabled, sf::Texture newTexture, sf::Vector2f newPos, float newHeight, float newWidth, sf::Vector2f newScale, float newRotation, SpriteOrigin newOrigin)']]]
];
