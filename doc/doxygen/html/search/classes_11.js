var searchData=
[
  ['rect_1466',['Rect',['../classsf_1_1_rect.html',1,'sf::Rect&lt; T &gt;'],['../class_rect.html',1,'Rect']]],
  ['rect_3c_20float_20_3e_1467',['Rect&lt; float &gt;',['../classsf_1_1_rect.html',1,'sf']]],
  ['rect_3c_20int_20_3e_1468',['Rect&lt; int &gt;',['../classsf_1_1_rect.html',1,'sf']]],
  ['rectangleshape_1469',['RectangleShape',['../classsf_1_1_rectangle_shape.html',1,'sf']]],
  ['renamectx_1470',['RenameCtx',['../struct_rename_ctx.html',1,'']]],
  ['renametoken_1471',['RenameToken',['../struct_rename_token.html',1,'']]],
  ['renderstates_1472',['RenderStates',['../classsf_1_1_render_states.html',1,'sf']]],
  ['rendertarget_1473',['RenderTarget',['../classsf_1_1_render_target.html',1,'sf']]],
  ['rendertexture_1474',['RenderTexture',['../classsf_1_1_render_texture.html',1,'sf']]],
  ['renderwindow_1475',['RenderWindow',['../classsf_1_1_render_window.html',1,'sf']]],
  ['request_1476',['Request',['../classsf_1_1_http_1_1_request.html',1,'sf::Http']]],
  ['response_1477',['Response',['../classsf_1_1_ftp_1_1_response.html',1,'sf::Ftp::Response'],['../classsf_1_1_http_1_1_response.html',1,'sf::Http::Response']]],
  ['reusablespace_1478',['ReusableSpace',['../struct_reusable_space.html',1,'']]],
  ['rowloadinfo_1479',['RowLoadInfo',['../struct_row_load_info.html',1,'']]],
  ['rowset_1480',['RowSet',['../struct_row_set.html',1,'']]],
  ['rowsetchunk_1481',['RowSetChunk',['../struct_row_set_chunk.html',1,'']]],
  ['rowsetentry_1482',['RowSetEntry',['../struct_row_set_entry.html',1,'']]]
];
