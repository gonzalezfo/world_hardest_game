var searchData=
[
  ['terrain_280',['Terrain',['../class_terrain.html#a7160a06ab07a86ed97d23374405e8ef6',1,'Terrain::Terrain()'],['../class_terrain.html#ab01140e53e5a77ac5294109d1e10b4b6',1,'Terrain::Terrain(const Terrain &amp;o)'],['../class_terrain.html#a3c338dc06d95531bae4fa65a3274fbe3',1,'Terrain::Terrain(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos, int newSides, float newRadius, float newRotation, sf::Vector2f newScale, float newThickness, const sf::Color &amp;newOutlineColor, const sf::Color &amp;newFillColor, uint8_t newSolid, TerrainType newType, sf::Vector2u newIndex)']]]
];
