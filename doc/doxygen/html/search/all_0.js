var searchData=
[
  ['background_0',['Background',['../class_background.html',1,'Background'],['../class_background.html#a05b686e4ce0cbdb3b1fa14a93fdf98a1',1,'Background::Background()'],['../class_background.html#ae405777feea917374cafe6b5b2e5edce',1,'Background::Background(const Background &amp;)'],['../class_background.html#a3b914237158e5f874a469d3478a2773b',1,'Background::Background(EntityTag newTag, uint8_t newEnabled, uint8_t newVertScroll, uint8_t newHorScroll, sf::Vector2f newVel, sf::Vector2f newPos, sf::Sprite newSprite, sf::Texture newTexture, int newHeight, int newWidth, SpriteOrigin newOrigin)']]],
  ['begindraw_1',['beginDraw',['../class_window.html#aa9a1a009e56ae8303efe7e90cea32399',1,'Window']]],
  ['border_5fthickness_5f_2',['border_thickness_',['../class_poly.html#a98be52a18c32a99b0d627007790f0447',1,'Poly::border_thickness_()'],['../class_rect.html#a8c6119b63b119f9869a172b65cd9a281',1,'Rect::border_thickness_()']]]
];
