var searchData=
[
  ['cacheinput_3',['cacheInput',['../class_input.html#ada7952e2943b202c14190c9c18e91118',1,'Input']]],
  ['can_5fmove_5f_4',['can_move_',['../class_player.html#af56f2690f5bef9d1c1c7d9fd4b7e245e',1,'Player']]],
  ['changeorigin_5',['changeOrigin',['../class_sprite.html#a1197a1b908160b2e1de6007667acf74f',1,'Sprite']]],
  ['character_5fsize_5f_6',['character_size_',['../class_label.html#adfc41253f25668d5f84140efa6fb868c',1,'Label']]],
  ['checknumberentities_7',['checkNumberEntities',['../class_game_manager.html#ad51a202019552d823587ae8424a90225',1,'GameManager']]],
  ['clock_5f_8',['clock_',['../class_game_clock.html#a20b0f7c403888b58d9e4842f14b55a56',1,'GameClock']]],
  ['createnewscene_9',['createNewScene',['../class_game_manager.html#adba313452057fb0bd68eb86879f94045',1,'GameManager']]],
  ['current_5fentity_5fcounter_5f_10',['current_entity_counter_',['../class_entity.html#a754ea1688b005cbaecd47a128037deb7',1,'Entity']]],
  ['current_5fscene_5f_11',['current_scene_',['../class_game.html#a33dd03e0ebfa1cf7a3e0fb822e4185f3',1,'Game']]]
];
