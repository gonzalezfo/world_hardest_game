var searchData=
[
  ['hbox_5f_47',['hbox_',['../class_poly.html#af4daed3548f85b97bc6c31f951fee66e',1,'Poly']]],
  ['height_5f_48',['height_',['../class_rect.html#ad00fc07f631ad2694b5accf9a7944a55',1,'Rect::height_()'],['../class_sprite.html#ac3720c70acc2ae294bbcc9e616b45357',1,'Sprite::height_()']]],
  ['hitbox_49',['Hitbox',['../class_hitbox.html',1,'Hitbox'],['../class_hitbox.html#a999b0be8486978b3dd7bcd001c7c3c03',1,'Hitbox::Hitbox()'],['../class_hitbox.html#a035fe74bb2e25b73b71f811d280c294b',1,'Hitbox::Hitbox(const Hitbox &amp;o)'],['../class_hitbox.html#ae68c00e6911789f33688edc0f5c34aab',1,'Hitbox::Hitbox(const sf::Vector3f *newPoints, int newNumberPoints)']]]
];
