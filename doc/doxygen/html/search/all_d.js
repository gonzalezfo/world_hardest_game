var searchData=
[
  ['radius_5f_88',['radius_',['../class_poly.html#a6f6aad1c225dc3ce1e81d91f7e535d24',1,'Poly']]],
  ['readscenes_89',['readScenes',['../class_game_manager.html#a675c64299a805cb367878e2fd186d5d9',1,'GameManager']]],
  ['rearrangeid_90',['rearrangeId',['../class_game_manager.html#a6401874c396a5d6d8b915b526d4b576f',1,'GameManager']]],
  ['rect_91',['Rect',['../class_rect.html',1,'Rect'],['../class_rect.html#a911e531b86de33734dd7de3456722115',1,'Rect::Rect()'],['../class_rect.html#a94b8cb7d93a546d0c08a83cd64be4aad',1,'Rect::Rect(const Rect &amp;)'],['../class_rect.html#a1b99fba204aa2b87dcf4eefbf4c52b0a',1,'Rect::Rect(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos, float newWidth, float newHeight, float newRotation, sf::Vector2f newScale, float newThickness, const sf::Color &amp;newOutlineColor, const sf::Color &amp;newFillColor, uint8_t newSolid)']]],
  ['restartclock_92',['restartClock',['../class_game_clock.html#a87b484d7eb1bdc9ad0cd84ce419fb154',1,'GameClock']]],
  ['right_5f_93',['right_',['../class_input.html#aece71e01bca5be1aab11cdb8a7a0cdaf',1,'Input']]],
  ['right_5fmouse_5f_94',['right_mouse_',['../class_input.html#a388330769e82ee0a673cf360d210c6b1',1,'Input']]],
  ['rotation_5f_95',['rotation_',['../class_poly.html#a60811d85ec27794056b64b52b98749f3',1,'Poly::rotation_()'],['../class_rect.html#afd19b45c8bb06a50df2a11230cdf89b7',1,'Rect::rotation_()'],['../class_sprite.html#acb3f686a53ff42328e8bf214293bbbe9',1,'Sprite::rotation_()']]]
];
