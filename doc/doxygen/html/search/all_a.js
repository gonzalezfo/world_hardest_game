var searchData=
[
  ['name_5f_71',['name_',['../class_scene.html#a59692bcc735a69aad68b7fc4f47613f6',1,'Scene']]],
  ['new_5fscene_5fname_5f_72',['new_scene_name_',['../class_select_scene.html#a330ae3ee792eb6a8ff90ea0b6e1e004e',1,'SelectScene']]],
  ['new_5fscene_5fselected_5fid_5f_73',['new_scene_selected_id_',['../class_editor_scene.html#ab527548dc9c3b48dcd04ee633f175662',1,'EditorScene::new_scene_selected_id_()'],['../class_play_scene.html#acfe56c9a48a3e16fee04e37f2f328619',1,'PlayScene::new_scene_selected_id_()']]],
  ['new_5fscene_5fselected_5ftype_5f_74',['new_scene_selected_type_',['../class_editor_scene.html#ae7a0ab6e615ad7c260041c0b97d0c3d8',1,'EditorScene::new_scene_selected_type_()'],['../class_play_scene.html#a7a6df05b2da11d1f641df27dd3a6d88b',1,'PlayScene::new_scene_selected_type_()']]],
  ['newscene_75',['newScene',['../class_game.html#af73dcf49b88fb265d1cd7f51e3814ab2',1,'Game']]],
  ['num_5fpoints_5f_76',['num_points_',['../class_hitbox.html#a0a8fd199cd9a61ca11bc462f737b3d08',1,'Hitbox']]],
  ['num_5fscenes_5f_77',['num_scenes_',['../class_select_scene.html#abe14a112a0d7cecd1ca3be74a97b3c4a',1,'SelectScene']]],
  ['number_5fdeaths_5f_78',['number_deaths_',['../class_game_manager.html#a051996707992df5e913a1487513bb849',1,'GameManager']]]
];
