/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#ifndef __SCENE_H__
#define __SCENE_H__ 1

#include "clock.h"
#include "input.h"
#include "window.h"
#include "entity.h"


const int kMaxScenes = 20;///<Max scenes to be loaded

/** @enum kSceneType
@brief The different types of scenes there are
*/
enum kSceneType {
  kSceneType_None,
  kSceneType_Editor,
  kSceneType_Play,
  kSceneType_Select,
};

/** @class Scene
@brief An abstract scene class
*/
class Scene {
 public:
  ///Default constructor
  Scene() : scene_id_(0){};
  ///Overloaded constructor
  Scene(int newID) : scene_id_(newID){};
  ///Purely virtual init
  virtual void init() = 0;
  ///Purely virtual update
  virtual void update() = 0;
  ///Purely virtual draw
  virtual void draw() = 0;

  int scene_id_;///<A unique scene id
  char name_[256];///<A specific scene name
  Entity* list_[kMaxEntities]; ///<List of entities present in the scene
  /** @brief Deletes all entities in the scene. It is called at the destructor.
      @return void
  */
  void deleteEntities(){
    int current_n_entities = Entity::current_entity_counter_;
    for(int i=0;i<current_n_entities;++i){
      delete list_[i];
    }
  }
  ///Destructor
  ~Scene(){
    deleteEntities();
  }
};

#endif //__SCENE_H__
