/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#ifndef __HITBOX_H__
#define __HITBOX_H__ 1

#include "SFML/System/Vector3.hpp"
#include "SFML/Graphics/CircleShape.hpp"

const float kSqrt2 = 1.41421356237f; ///< The square root of 2

/** @class Hitbox
@brief A class which contains the hitbox for a poly, to calculate collisions
*/
class Hitbox {
 public:
  //Methods
  ///Default constructor
  Hitbox();
  ///Copy constructor
  Hitbox(const Hitbox& o);
  ///Overloaded constructor
  Hitbox(const sf::Vector3f* newPoints, int newNumberPoints);

  /** @brief Initializes with default values
  @return void
  */
  void init();

  /** @brief Initializes with parameter values
  @return void
  */
  void init(const sf::Vector3f* newPoints, int newNumberPoints);

  ///Destructor
  ~Hitbox();

  /** @brief Updates the hitbox given a circle shape as reference
  @param shape An sfml circleshape that the hitbox will update to
  @return void
  */
  void update(sf::CircleShape& shape);

  /** @brief Given another hitbox it checks for collision with this hitbox
  @param other_hbox The hitbox to check with
  @return bool If the two hitboxes are intersecting
  */
  bool intersecCollision(const Hitbox& other_hbox);

  /** @brief Draws the hitbox for execution time checks
  @return void
  */
  void draw();

  //Data
  sf::Vector3f points_[30];///< A vector of the hitbox's points (max 30)
  int num_points_;///< The amount of points this hitbox has (max 30)
};




#endif //__HITBOX_H__
