/*Jesús González Fonseca*/


#ifndef __POLY_H__
#define __POLY_H__ 1

#include "SFML/System/Vector2.hpp"
#include "SFML/Graphics/Color.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "SFML/Graphics/CircleShape.hpp"
#include "entity.h"

/** @class Poly
@brief A general polygon class
*/
class Poly : public Entity {
 public:
  //Methods
  ///Default constructor
  Poly();
  ///Copy constructors
  Poly(const Poly&);
  ///Overloaded constructor
  Poly(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
       int newSides, float newRadius, float newRotation,
       sf::Vector2f newScale, float newThickness,
       const sf::Color& newOutlineColor, const sf::Color& newFillColor,
       uint8_t newSolid);
  /** @brief Initializes the polygon with default values
  @return void
  */
  void init() override;
  /** @brief Initializes the player with parameter values
  @return void
  */
  void init(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
            int newSides, float newRadius, float newRotation,
            sf::Vector2f newScale, float newThickness,
            const sf::Color& newOutlineColor, const sf::Color& newFillColor,
            uint8_t newSolid);
   /** @brief Updates the polygon and its hitbox simultaneoulsy
  @return void
  */
  void update() override;
  /** @brief Draws the polygon into the window class
  @return void
  */
  void draw() override;
  ///Destructor
  virtual ~Poly();
  ///Sets a specific radius
  void setRadius(float newRadius);
  ///Sets a specific number of sides
  void setPointCount(int newSides);
  ///Sets a given position
  void setPosition(sf::Vector2f newPos) override;
  ///Sets a given rotation
  void setRotation(float newRotation);
  ///Sets a given scale
  void setScale(sf::Vector2f newScale);
  ///Sets a given outline thickness
  void setOutlineThickness(float newThickness);
  ///Sets a given outline color
  void setOutlineColor(const sf::Color& newOutlineColor);
  ///Sets a given fill color
  void setFillColor(const sf::Color& newFillColor);

  //Data
  sf::CircleShape poly_;///<The actual sfml circle shape
  Hitbox hbox_;///< A hitbox for collisions

  sf::Vector2f pos_;  ///<Position coordinates
  int sides_; ///<Number of sides
  float radius_; ///<Radius of the polygon
  float rotation_; ///<Rotation of the polygon
  sf::Vector2f scale_; ///<Scale in x and y axis
  float border_thickness_; ///<Border thickness
  sf::Color outline_color_; ///<Color of the border
  sf::Color fill_color_; ///<Color of the inside
  uint8_t solid_; ///<Is the ploygon colored on the inside
};



#endif // __POLY_H__
