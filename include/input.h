/*Jesús González Fonseca*/


#ifndef __INPUT_H__
#define __INPUT_H__ 1


#include <stdint.h>
#include "SFML/Window/Event.hpp"

/** @enum PeripheralType
@brief The different types of peripherals we accept
*/
enum PeripheralType {
  kPeripheralType_Keyboard,
  kPeripheralType_Mouse
};

/** @class Input
@brief A singleton Input to update the game with
*/
class Input {
 public:
  //Methods
  ///Singleton's instance
  static Input& Instance();

  /** @brief Initializes the input with default values
      @return void
  */
  void init();
  /** @brief Gets all inputs into the class
      @param event The event we have to parse
      @return void
  */
  void getInput(sf::Event event);
  /** @brief Cache last frame's input into this frame's
      @return void
  */
  void cacheInput();

  /** @brief Detects if this is the first frame a button is down
      @param peripheral The kind of peripheral we are porking with
      @param type What key we want to know
      @return void
  */
  bool isButtonDown(PeripheralType peripheral, int type);
  /** @brief Detects if this is the first frame a button is up
      @param peripheral The kind of peripheral we are porking with
      @param type What key we want to know
      @return void
  */
  bool isButtonUp(PeripheralType peripheral, int type);
  ///Destructor
  ~Input();

  //Data
  uint8_t esc_; ///<Escape key
  uint8_t left_mouse_;  ///<Left mouse click
  uint8_t right_mouse_;  ///<Right mouse click
  uint8_t up_;  ///<Up arrow key
  uint8_t down_; ///<Down arrow key
  uint8_t left_; ///<Left arrow key
  uint8_t right_; ///<Right arrow key

 private:
  //What the values were last frame (for key down and key up functions)
  uint8_t esc_last_; ///<Escape key from last frame
  uint8_t left_mouse_last_;///<Left mouse click from last frame
  uint8_t right_mouse_last_;///<Right mouse click from last frame
  uint8_t up_last_;///<Up arrow key from last frame
  uint8_t down_last_;///<Down arrow key from last frame
  uint8_t left_last_;///<Left arrow key from last frame
  uint8_t right_last_;///<Right arrow key from last frame

  ///Default constructor
  Input();
  ///Copy constructor
  Input(const Input& o);
};

#endif //__INPUT_H__
