//Author: Jesús González <gonzalezfo@esat-alumni.com>

#ifndef __WINDOW_H__
#define __WINDOW_H__ 1

#include <string.h>
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"

/** @class Input
@brief A singleton Window to wrap an sfml Window class
*/
class Window{
 public:
  //Methods
    ///Singleton's instance
  static Window& Instance();

  /** @brief Initializes the window with default values
      @return void
  */
  void init();
  /** @brief Initializes the window with parameter values
      @return void
  */
  void init(sf::Vector2u newSize, std::string newTitle);
  /** @brief Draws a clear color background
      @param newColor The color to clear with
      @return void
  */
  void beginDraw(sf::Color newColor);
  /** @brief Displays all entities drawn on the window
      @return void
  */
  void endDraw();
  /** @brief An sfml draw wrap
      @param drawable_ A drawable sfml entity
      @return void
  */
  void draw(sf::Drawable& drawable_);
  /** @brief Closes the window
      @return void
  */
  void destroy();

  //Data
  sf::Vector2u window_size_;///<Window height and width
  std::string window_title_;///<Window title
  sf::RenderWindow window_;///<The sfml window

 private:
  ///Default constructor
  Window();
  ///Copy constructor
  Window(const Window& w);
  ///Overloaded constructor
  Window(sf::Vector2u newSize, std::string newTitle, sf::RenderWindow& newWindow);
};


#endif //__WINDOW_H__
