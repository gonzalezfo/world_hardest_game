/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#ifndef __TERRAIN_H__
#define __TERRAIN_H__ 1

#include "poly.h"

const int kMaxColumns = 25;///const used to specify the max of columns of terrain

/** @enum TerrainType
@brief The different types of terrains there are
*/
enum TerrainType {
  kTerrainType_None,
  kTerrainType_Wall,
  kTerrainType_Start,
  kTerrainType_Finish,
  kTerrainType_Path
};

/** @class Terrain
@brief  A general terrain class
*/
class Terrain : public Poly {
 public:
  //Methods
  ///Default constructor
  Terrain();
  ///Copy constructor
  Terrain(const Terrain& o);
  ///Overloaded constructor
  Terrain(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
          int newSides, float newRadius, float newRotation,
          sf::Vector2f newScale, float newThickness,
          const sf::Color& newOutlineColor, const sf::Color& newFillColor,
          uint8_t newSolid, TerrainType newType, sf::Vector2u newIndex);
    /** @brief Initializes the terrain with default values
        @return void
    */
  void init() override;
  /** @brief Initializes the terrain with parameter values
      @return void
  */
  void init(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
            int newSides, float newRadius, float newRotation,
            sf::Vector2f newScale, float newThickness,
            const sf::Color& newOutlineColor, const sf::Color& newFillColor,
            uint8_t newSolid, TerrainType newType, sf::Vector2u newIndex);
  ///Destructor
  virtual ~Terrain();

  /** @brief Updates the terrain
      @return void
  */
  void update() override;
  TerrainType terrain_type_; ///<What specific terrain type it is
  sf::Vector2u index_;///<The position in the grid of terrains
};

#endif //__TERRAIN_H__
