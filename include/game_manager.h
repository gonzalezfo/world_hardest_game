//Author: Jesús González <gonzalezfo@esat-alumni.com>

#ifndef __GAME_MANAGER_H__
#define __GAME_MANAGER_H__ 1

 #include "sqlite3.h"
 #include "scene.h"
 #include "game.h"
 #include "play_scene.h"

class Game;
/** @class GameManager
@brief A singleton GameManager that controlls basic game functions
*/
class GameManager{
 public:

  ///Singleton's instance
    static GameManager& Instance();

    /** @brief Checks for max id in database
        @return int The maximum id found (max number of entities)
    */
    int checkNumberEntities();
    /** @brief Rearranges ids in database to stop fragmentation
        @return void
    */
    void rearrangeId();
    ///Loads a poly from the database
    void loadPoly(int id_, int tag_, int enabled_, char sql[254],
                  sqlite3_stmt *res, const char *tail,Scene* scene,
                  bool terrain_grid[kMaxColumns][kMaxColumns]);
    /** @brief Loads all entities from the database into a given scene
    @param scene The scene we want to load entities to
    */
    void loadSceneEntities(Scene* scene);
    /** @brief Writes entities into the database
    @param scene The scene we want to write entities from
    */
    void writeEntities(Scene* scene);
    /** @brief Writes a poly into the database
    @param poly_ A copy of a poly we want to write
    @param tag_ The tag of that poly for further writing
    */
    void writePoly(Poly* poly_, int tag_);
    /** @brief Reads all available scenes from the
    @param poly_ A copy of a poly we want to write
    @param tag_ The tag of that poly for further writing
    @return int number of scenes
    */
    int readScenes(PlayScene[kMaxScenes]);
    /** @brief Saves a scene in the database
    @param name The name that the scene is going to show in the menu later
    */
    void createNewScene(char name[128]);

    Game* game_;//<A pointer to the current game that is being played
    int number_deaths_;///<The amount of times the user has died this execution
    sqlite3 *db;///< The sqlite database

 private:
    ///Default constructor
    GameManager();
    ///Copy constructor
    GameManager(const GameManager& o);
    ///Destructor
    ~GameManager();
};


#endif //__GAME_MANAGER_H__
