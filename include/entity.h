/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#ifndef __ENTITY_H__
#define __ENTITY_H__ 1

#include <stdint.h> //Header to use specific sized variables
#include "hitbox.h"
#include "SFML/System/Vector2.hpp"

const int kMaxEntities = 1000000;///< An entities maximum

/** @enum kEntityTag
@brief What kind of entities there are
*/
enum EntityTag {
  kEntityTag_Untagged = -1,
  kEntityTag_Enemy,
  kEntityTag_Terrain,
  kEntityTag_Player,
  kEntityTag_Text,
  kEntityTag_Sprite,
  kEntityTag_Background
};

/** @class Entity
@brief The most basic abstract class, from where most other clases inherit
*/
class Entity {
 public:
  //Methods
  ///Default constructor
  Entity();
  ///Copy constructor
  Entity(const Entity& o);

  ///Overloaded constructor
  Entity(EntityTag newTag, uint8_t newEnabled);

  /** @brief Initializes the entity with default values
  @return void
  */
  virtual void init();

  /** @brief Initializes the entity with parameter values
  @return void
  */
  void init(EntityTag newTag, uint8_t newEnabled);

  /** @brief A virtual setter for position
  @param pos An sfml vector2 float for x and y position
  @return void
  */
  virtual void setPosition(sf::Vector2f pos);

  /** @brief A virtual update for the entity
  @return void
  */
  virtual void update();
  /** @brief A virtual draw for the entity
  @return void
  */
  virtual void draw();

  ///Default destructor
  virtual ~Entity();

  /** @brief An id getter
  @return uint32_t The entity's id
  */
  uint32_t id();

  //Data
  EntityTag tag_; ///<To assign similar entities the same tag
  uint8_t enabled_; ///<An unsigned int of exactly 8 bits that determines if an entity is enabled or not

  static int entity_counter_; ///<A static entity counter for how many entities have been created this execution
  uint32_t id_;///<A specific id, unique for each entity
  static uint32_t current_entity_counter_; ///<How many entities are currently on screen
};

#endif //__ENTITY_H__
