/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */


 #ifndef __SPRITE_H__
 #define __SPRITE_H__ 1

 #include "SFML/Graphics/Sprite.hpp"
 #include "SFML/Graphics/Texture.hpp"
 #include "entity.h"

 /** @enum SpriteOrigin
 @brief The different types of origins sprites may have
 */
 enum SpriteOrigin {
   kSpriteOrigin_None,
   kSpriteOrigin_File,
   kSpriteOrigin_Memory,
   kSpriteOrigin_Copy
 };

 /** @class Sprite
 @brief  A sprite class wrapping
 */
 class Sprite : public Entity {
  public:
   //Methods
   ///Default constructor
   Sprite();
   ///Copy constructor
   Sprite(const Sprite& o);
   ///Overloaed constructor
   Sprite(EntityTag newTag, uint8_t newEnabled, sf::Texture newTexture,
          sf::Vector2f newPos, float newHeight,
          float newWidth, sf::Vector2f newScale, float newRotation,
          SpriteOrigin newOrigin);
     /** @brief Initializes the sprite with default values
         @return void
     */
   void init() override;
   /** @brief Initializes the sprite from a file
      @param source File source
       @return void
   */
   void init(const char* source);

   /** @brief Sets the sprite position
      @param newPos The new position to set
       @return void
   */
   void setPosition(sf::Vector2f newPos);
   /** @brief Sets the sprite rotation
      @param newRot The new rotation to set
       @return void
   */
   void setRotation(float newRot);
   /** @brief Sets the sprite scale
      @param newScale The new scale to set
       @return void
   */
   void setScale(sf::Vector2f newScale);

   /** @brief Gets the sprite height
       @return height Returns the sprite height
   */
   float getHeight();
   /** @brief Gets the sprite width
       @return width Returns the sprite width
   */
   float getWidth();
   /** @brief Gets a specific pixel color
      @param pixel The pixel position in the sprite
       @return color Returns the pixel color
   */
   sf::Color getPixel(sf::Vector2u pixel);

   /** @brief Changes a sprite's origin
      @param newOrigin The sprite's new origin
       @return void
   */
   void changeOrigin(SpriteOrigin newOrigin);
   /** @brief Updates the sprite
       @return void
   */
   void update() override;
   /** @brief Draws the sprite into the window
       @return void
   */
   void draw() override;
   ///Destructor
   ~Sprite();

   //Data
   sf::Sprite sprite_; ///<SFML sprite
   sf::Texture texture_; ///<SFML texture
   sf::Vector2f pos_;///<Position coordinates
   float height_; ///<Height of the sprite
   float width_; ///<Width of the sprite
   sf::Vector2f scale_; ///<Scale of the sprite
   float rotation_; ///<Sprite rotation
   SpriteOrigin origin_;///<Where the sprite comes from
 };


 #endif //__SPRITE_H__
