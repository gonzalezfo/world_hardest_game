/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#ifndef __RECT_H__
#define __RECT_H__ 1

#include "SFML/System/Vector2.hpp"
#include "SFML/Graphics/Color.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "entity.h"

/** @class Rect
@brief A general rectangle class
*/
class Rect : public Entity {
 public:
  //Methods
  ///Default constructor
  Rect();
  ///Copy constructor
  Rect(const Rect&);
  ///Overloaded constructor
  Rect(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
       float newWidth, float newHeight, float newRotation,
       sf::Vector2f newScale, float newThickness,
       const sf::Color& newOutlineColor, const sf::Color& newFillColor,
       uint8_t newSolid);
/** @brief Initializes the rectangle with default values
    @return void
    */
  void init() override;
  /** @brief Initializes the polygon with parameter values
  @return void
  */
  void init(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
            float newWidth, float newHeight, float newRotation,
            sf::Vector2f newScale, float newThickness,
            const sf::Color& newOutlineColor, const sf::Color& newFillColor,
            uint8_t newSolid);
    /** @brief Draws the rectangle into the window class
    @return void
    */
  void draw() override;
  ///Destructor
  ~Rect();

  //Data
  sf::Vector2f pos_;///<Position coordinates
  float height_; ///<Height of the rectangle
  float width_; ///<Width of the rectangle
  float rotation_;///<Rotation of the polygon
  sf::Vector2f scale_;///<Scale in x and y axis
  float border_thickness_;///<Border thickness
  sf::Color outline_color_;///<Color of the border
  sf::Color fill_color_;///<Color of the inside
  uint8_t solid_;///<Is the ploygon colored on the inside
};

#endif //__RECT_H__
