/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

 #ifndef __BACKGROUND_H__
 #define __BACKGROUND_H__ 1

 #include <stdint.h>
 #include "SFML/System/Vector2.hpp"
 #include "SFML/Graphics/Sprite.hpp"
 #include "entity.h"
#include "sprite.h"

/** @class Background
@brief  A moving background
*/
class Background: public Entity {
 public:
  ///Default constructor
  Background();

  ///Copy constructor
  Background(const Background&);

  ///Overloaded constructor
  Background(EntityTag newTag, uint8_t newEnabled, uint8_t newVertScroll,
             uint8_t newHorScroll, sf::Vector2f newVel, sf::Vector2f newPos,
             sf::Sprite newSprite, sf::Texture newTexture, int newHeight,
             int newWidth, SpriteOrigin newOrigin);

  //Destructor
  virtual ~Background();

  /** @brief Initializes the background with default values
      @return void
  */
  void init() override;

  /** @brief Initializes the background with these values
      @param file_name The file source root
      @param window_measures The window's heigth and width
      @return void
  */
  void init(const char* file_name, sf::Vector2u window_measures);

  /** @brief Updates the background
      @return void
  */
  void update() override;

  /** @brief Draws the background
      @return void
  */
  void draw() override;

  uint8_t scrolls_horizontally_;///< Does the background scroll horizontally
  uint8_t scrolls_vertically_;///< Does the background scroll vertically
  sf::Vector2f velocity_;///< Velocity in both x and y
  sf::Vector2f pos_; ///< X and Y position

 private:
  sf::Sprite sprite_; ///< The background sprite
  sf::Texture texture_; ///< The background texture
  int window_height_; ///< The window height
  int window_width_; ///< The window width
  SpriteOrigin origin_; ///< Where does the sprite come from
};

 #endif //__BACKGROUND_H__
