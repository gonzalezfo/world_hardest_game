/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "entity.h"
#include "scene.h"
#include "terrain.h"
#include "sqlite3.h"


#ifndef __PLAY_SCENE_H__
#define __PLAY_SCENE_H__ 1

/** @class PlayScene
@brief  A scene where you can play the game
*/
class PlayScene : public Scene {
 public:
  ///Default constructor
  PlayScene();
    ///Overloaded constructor
  PlayScene(int newId);
  ///Destructor
  ~PlayScene();

  /** @brief Initializes the scene loading all entities
      @return void
  */
  void init() override;

  /** @brief Updates the scene
      @return void
  */
  void update() override;

  /** @brief Draws all entities in the scene
      @return void
  */
  void draw() override;

  /** @brief Draws ImGui main menu on the top of the screen
      @return void
  */
  void mainMenu();

  int new_scene_selected_id_;///<If scene has been changed, default value -1
  kSceneType new_scene_selected_type_; ///<What the scene has been changed to
};

#endif //__PLAY_SCENE_H__
