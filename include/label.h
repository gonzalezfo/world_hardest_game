//Author: Jesús González <gonzalezfo@esat-alumni.com>

 #ifndef __LABEL_H__
 #define __LABEL_H__ 1

#include "SFML/Graphics/Text.hpp"
#include "SFML/System/Vector2.hpp"
#include "SFML/Graphics/Color.hpp"
#include "entity.h"

/** @class Label
@brief A label wrapping of sfml text
*/
class Label : public Entity {
 public:
  //Methods
  ///Default constructor
  Label();
  ///Copy constructor
  Label(const Label& o);
  ///Overloaded constructor
  Label(EntityTag newTag, uint8_t newEnabled, unsigned int newSize,
        const sf::String& newString, const sf::String& newSource,
        const sf::Color& newOutlineColor, const sf::Color& newFillColor,
        float newThickness, float newLetterSpacing, float newLineSpacing);
  ///Destructor
  virtual ~Label();
  /** @brief Initializes the label with default values
      @return void
  */
  void init() override;
  /** @brief Initializes the input with parameter values
      @return void
  */
  void init(EntityTag newTag, uint8_t newEnabled, unsigned int newSize,
            const sf::String& newString, const sf::String& newSource,
            const sf::Color& newOutlineColor,
            const sf::Color& newFillColor, float newThickness,
            float newLetterSpacing, float newLineSpacing);
  /** @brief Loads font from a file
  @param newSource A font string
  @return void
  */
  void loadFont(const sf::String& newSource);
  /** @brief Sets the font size
  @param newSize The text's new size
  @return void
  */
  void setFontSize(unsigned int newSize);
  /** @brief Sets the font string
  @param newString The text's new string
  @return void
  */
  void setString(const sf::String& newString);
  /** @brief Sets the font outline color
  @param newOutlineColor The text's new outline color
  @return void
  */
  void setOutlineColor(const sf::Color& newOutlineColor);
  /** @brief Sets the font fill color
  @param newFillColor The text's new fill color
  @return void
  */
  void setFillColor(const sf::Color& newFillColor);
  /** @brief Sets the font thickness
  @param newThickness The text's new thickness
  @return void
  */
  void setThickness(float newThickness);
  /** @brief Sets the font letter spacing
  @param newLetterSpacing The text's new letter spacing
  @return void
  */
  void setLetterSpacing(float newLetterSpacing);
  /** @brief Sets the font line spacing
  @param newLineSpacing The text's new line spacing
  @return void
  */
  void setLineSpacing(float newLineSpacing);
  /** @brief Gets a specific string character's position in the window
  @param newIndex The index the character is in
  @return position An sfml vector2f that determines the character's spacial position
  */
  sf::Vector2f getCharacterPosition(std::size_t newIndex);
  /** @brief Updates the label
  @return void
  */
  void update() override;
  /** @brief Draws the label
  @return void
  */
  void draw() override;


  //Data
  sf::Text text_;///< The sfml text we wrap
  sf::Font font_;///< The text font

  unsigned int character_size_;///<Character size
  sf::String string_; ///<Character text
  sf::String file_source_;///<Font file source
  sf::Color outline_color_;///<Font outline color
  sf::Color fill_color_;///<Font fill color
  float thickness_;///<Font thickness
  float letter_spacing_factor_;///<Text letter spacing
  float line_spacing_factor_;///<Text line spacing
  std::size_t index_;///<Last font index searched
};


 #endif //__LABEL_H__
