//Author: Jesús González <gonzalezfo@esat-alumni.com>

#ifndef __CLOCK_H__
#define __CLOCK_H__ 1

#include "SFML/System.hpp"

/** @class GameClock
@brief  A singleton wrapping of sfml::clock
*/
class GameClock {
 public:
  ///Singleton's instance
  static GameClock& Instance();

  /** @brief Initializes the clock with default values
      @return void
  */
  void init();

  /** @brief Restarts the clock, is called at the end of each frame
      @return void
  */
  void restartClock();

  /** @brief A delta_time_ getter
      @return float delta_time_
  */
  float deltaTime();

  /** @brief A wrapper for clock elapsedTime
      @return float delta_time_
  */
  sf::Time elapsedTime();

  ///Destructor
  ~GameClock();

  sf::Clock clock_;///<An sfml clock

  float delta_time_; ///<How much time did the last frame take
 private:
  ///Default constructor
  GameClock();
};


#endif //__CLOCK_H__
