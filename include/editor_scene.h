/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "entity.h"
#include "scene.h"
#include "terrain.h"



#ifndef __EDITOR_SCENE_H__
#define __EDITOR_SCENE_H__ 1

/** @class EditorScene
@brief  A scene where you can edit the game in real time
*/
class EditorScene : public Scene {
 public:
  ///Default constructor
  EditorScene();

  ///Overloaded constructor
  EditorScene(int newId);

  ///Destructor
  ~EditorScene();

  /** @brief Initializes the scene with a base of wall terrain
      @return void
  */
  void init() override;

  /** @brief Updates the scene
      @return void
  */
  void update() override;

  /** @brief Draws all entities in the scene
      @return void
  */
  void draw() override;

  Entity* selected_;///< What entity has been selected to be placed

  TerrainType terrain_type_; ///<Special selection for terrain because of tiling

  int new_scene_selected_id_;///<If scene has been changed, default value -1
  kSceneType new_scene_selected_type_; ///<What the scene has been changed to

 protected:
  /** @brief Draws ImGui main menu on the top of the screen
      @return void
  */
  void mainMenu();
  /** @brief The editor tool to add and remove entities
      @return void
  */
  void editionTool();
  /** @brief Fills the grid with terrain
      @return void
  */
  void fillTerrain();
  /** @brief Fills the grid with empty terrain
      @return void
  */
  void fillEmptyTerrain();
};

#endif //__EDITOR_SCENE_H__
