/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#ifndef __GAME_H__
#define __GAME_H__ 1

#include "SFML/Graphics.hpp"
#include "player.h"
#include "input.h"
#include "window.h"
#include "enemy.h"
#include "scene.h"

/** @class Game
@brief A game class that controls the main flux of the game
*/
class Game {
 public:
  ///Default constructor
  Game();
  /** @brief Initializes window and ImGui
  @return void
  */
  void init();

  /** @brief Main loop for the whole program
  @return void
  */
  void mainLoop();

  /** @brief Free and release of all main variables of the program
  @return void
  */
  void finish();

  /** @brief Changes curent scene to whatever the new scene id is, opening as the type specified
  @param scene_id What the new scene's id is
  @param type How the new scene should be created as
  @return void
  */
  void newScene(int scene_id, kSceneType type);

  ///Default destructor
  ~Game();

 protected:
  /** @brief Handles user input into singleton
  @return void
  */
  void input();
  /** @brief Updates the game state given certain inputs
  @return void
  */
  void update();
  /** @brief Draws on the screen
  @return void
  */
  void draw();

  //Data
  Scene *current_scene_;///<What the scene is currently
};

#endif //__GAME_H__
