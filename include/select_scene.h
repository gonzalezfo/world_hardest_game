/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "entity.h"
#include "scene.h"
#include "terrain.h"
#include "play_scene.h"

#ifndef __SELECT_SCENE_H__
#define __SELECT_SCENE_H__ 1

/** @class SelectScene
@brief An level selection scene
*/
class SelectScene : public Scene {
 public:
  ///Default constructor
  SelectScene();
  ///Overloaded constructor
  SelectScene(int newId);
  ///Destructor
  ~SelectScene();

  /** @brief Initializes the scene loading all scenes
      @return void
  */
  void init() override;
  /** @brief Searches for user updates
      @return void
  */
  void update() override;
  /** @brief Draws imgui
      @return void
  */
  void draw() override;

  PlayScene scene_list_[kMaxScenes]; ///<We use play scenes as Scenes are abstract classes
  int num_scenes_; ///<Number of available scenes
  int selected_scene_id_; ///<What scene has been selected (default -1)
  kSceneType selected_scene_type_; ///<How it has been decided to load
  char new_scene_name_[128];///<Name of the new scene to create

 protected:
  /** @brief Selects what scene the user wants to load
      @return void
  */
  void selectScene();
};

#endif //__SELECT_SCENE_H__
