//Author: Jesús González <gonzalezfo@esat-alumni.com>


#ifndef __ENEMY_H__
#define __ENEMY_H__ 1

#include "SFML/System/Vector2.hpp"
#include "SFML/System/Time.hpp"
#include "poly.h"

/** @class Enemy
@brief Enemy class
*/
class Enemy : public Poly {
 public:
  //Methods
  ///Default constructor
  Enemy();
  ///Copy constructor
  Enemy(const Enemy&);

  ///Overloaded constructor
  Enemy(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos, int newSides,
        float newRadius, float newRotation, sf::Vector2f newScale,
        float newThickness, const sf::Color& newOutlineColor,
        const sf::Color& newFillColor, uint8_t newSolid,
        sf::Vector2f newVelocity, float newSpeed_);

  /** @brief Initializes the enemy with default values
  @return void
  */
  void init() override;

  /** @brief Initializes the enemy with parameter values
  @return void
  */
  void init(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
            int newSides, float newRadius, float newRotation,
            sf::Vector2f newScale, float newThickness,
            const sf::Color& newOutlineColor, const sf::Color& newFillColor,
            uint8_t newSolid, sf::Vector2f newVelocity, float newSpeed_);

  /** @brief Moves an enemy given its velocity
  @return void
  */
  void move();

  /** @brief Updates the enemy
  @return void
  */
  void update() override; //Update the enemy

  /** @brief Kills the enemy (sets its enabled to 0)
  @return void
  */
  void die();

  //Destructor
  virtual ~Enemy();

  //Data
  sf::Vector2f velocity_; ///<What we add to pos_ to move the enemy
  float speed_; ///<What we multiply deltaTime with to get velocity


  static int total_enemies_;  ///<Counter for the factory
};

#endif //__ENEMY_H__
