/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#ifndef __PLAYER_H__
#define __PLAYER_H__ 1

#include "SFML/System/Vector2.hpp"
#include "SFML/System/Time.hpp"
#include "poly.h"
#include "input.h"

/** @class Player
@brief The player's class
*/
class Player : public Poly {
 public:
  //Methods
    ///Default constructor
  Player();
    ///Copy constructor
  Player(const Player& o);
    ///Overloaded constructor
  Player(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
         int newSides, float newRadius, float newRotation,
         sf::Vector2f newScale, float newThickness,
         const sf::Color& newOutlineColor, const sf::Color& newFillColor,
         uint8_t newSolid, sf::Vector2f newVelocity,
         float newSpeed_);

  /** @brief Initializes the player with default values
  @return void
  */
  void init() override;
  /** @brief Initializes the player with parameter values
  @return void
  */
  void init(EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
            int newSides, float newRadius, float newRotation,
            sf::Vector2f newScale, float newThickness,
            const sf::Color& newOutlineColor, const sf::Color& newFillColor,
            uint8_t newSolid, sf::Vector2f newVelocity,
            float newSpeed_);
    /** @brief Moves an player given its velocity
    @return void
    */
  void move();
  /** @brief Updates the player
  @return void
  */
  void update() override;
  /** @brief Kills the player (sets its enabled to 0)
  @return void
  */
  void die();
  //Destructor
  virtual ~Player();

  //Data
  sf::Vector2f velocity_; ///<What we add to pos_ to move the player
  float speed_; ///<What we multiply deltaTime with to get velocity
  static int player_counter_;///<Counter for the factory (max of one)
  bool can_move_;///<If the player can move this frame (has not collided)
};

#endif //__PLAYER_H__
