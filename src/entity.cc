/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "entity.h"

int Entity::entity_counter_ = -1;
uint32_t Entity::current_entity_counter_ = 0;

Entity::Entity() {
  tag_ = kEntityTag_Untagged;
  enabled_ = 1; //enabled by default
  Entity::entity_counter_ += 1;
  id_ = Entity::entity_counter_;
  ++Entity::current_entity_counter_;
}

Entity::Entity (const Entity& o) {
  tag_ = o.tag_;
  enabled_ = o.enabled_;
  Entity::entity_counter_ += 1;
  id_ = Entity::entity_counter_;
  ++Entity::current_entity_counter_;
}

Entity::Entity (EntityTag newTag, uint8_t newEnabled) {
  tag_ = newTag;
  enabled_ = newEnabled;
  Entity::entity_counter_ += 1;
  id_ = Entity::entity_counter_;
  ++Entity::current_entity_counter_;
}

void Entity::init() {
  tag_ = kEntityTag_Untagged;
  enabled_ = 1;
}

void Entity::init(EntityTag newTag, uint8_t newEnabled) {
  tag_ = newTag;
  enabled_ = newEnabled;
}

void Entity::update() {}

void Entity::draw() {}

Entity::~Entity() {
  --Entity::current_entity_counter_;
}

void Entity::setPosition(sf::Vector2f pos) {}
