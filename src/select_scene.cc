/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "select_scene.h"
#include "play_scene.h"
#include "imgui.h"
#include "imgui-SFML.h"
#include "player.h"
#include "enemy.h"
#include "terrain.h"
#include "game_manager.h"

#include "SFML/Graphics/Color.hpp"
#include "SFML/System/Vector2.hpp"

SelectScene::SelectScene() {
  num_scenes_ = 0;
  selected_scene_id_ = -1;
  selected_scene_type_ = kSceneType_None;
}

SelectScene::SelectScene (int newId) {
  num_scenes_ = 0;
  selected_scene_id_ = -1;
  selected_scene_type_ = kSceneType_None;
  scene_id_ = newId;
}


void SelectScene::init() {
  num_scenes_=GameManager::Instance().readScenes(scene_list_);
  memset(new_scene_name_, '\0', IM_ARRAYSIZE(new_scene_name_));
}

void SelectScene::selectScene() {
  ImGui::Begin("Available Scenes");
  ImGui::InputText("--> New scene name", new_scene_name_,127);
  if (ImGui::Button("Create") && new_scene_name_[0] != '\0') {
      GameManager::Instance().createNewScene(new_scene_name_);
  num_scenes_ = GameManager::Instance().readScenes(scene_list_);
  }

  for (int i = 0; i < num_scenes_; ++i) {
    ImGui::PushID(i);
    ImGui::Text("%d: %s", i + 1, scene_list_[i].name_);
    if (ImGui::Button("Play")) {
      selected_scene_id_ = scene_list_[i].scene_id_;
      selected_scene_type_ = kSceneType_Play;
    }
    ImGui::SameLine(200);
    if (ImGui::Button("Edit")) {
      selected_scene_id_ = scene_list_[i].scene_id_;
      selected_scene_type_ = kSceneType_Editor;
    }
    ImGui::PopID();
  }
  ImGui::End();
}

void SelectScene::update() {
  ImGui::SFML::Update(Window::Instance().window_,GameClock::Instance().elapsedTime());
  selectScene();

  if (-1 != selected_scene_id_) {
    GameManager::Instance().game_->newScene(selected_scene_id_, selected_scene_type_);
  }
}

void SelectScene::draw(){}
