//Author: Jesús González <gonzalezfo@esat-alumni.com>

#define _CRT_SECURE_NO_WARNINGS 1

#include "game_manager.h"
#include "sqlite3.h"
#include "stdio.h"
#include "play_scene.h"
#include "player.h"
#include "enemy.h"
#include "terrain.h"


GameManager& GameManager::Instance() {
  static GameManager* inst = nullptr;
  if(inst == nullptr){
    inst = new GameManager();
  }
  return *inst;
}

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i < argc; i++) {}
   return 0;
}

GameManager::GameManager() {
  number_deaths_ = 0;

  //Open DB
  if (sqlite3_open("../data/hardest_db.db", &db)) {
    sqlite3_close(db);
  }
  Entity::entity_counter_ = checkNumberEntities();
}

int GameManager::checkNumberEntities() {
  sqlite3_stmt *res;
  const char *tail;
  char sql[256];
  int current_id_ = 0;
  int max_id_ = 0;

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "SELECT * FROM ENTITY");

  sqlite3_prepare_v2(db, sql, 128, &res, &tail);

  while (sqlite3_step(res) == SQLITE_ROW) {
    current_id_ = sqlite3_column_int(res, 0);
    if (current_id_ > max_id_) {
      max_id_ = current_id_;
    }
  }
  sqlite3_finalize(res);
  return max_id_;
}

void GameManager::loadPoly(int id_, int tag_, int enabled_, char sql[254],
                            sqlite3_stmt *res, const char *tail, Scene* scene,
                            bool terrain_grid[kMaxColumns][kMaxColumns]){
  sf::Vector2f pos_;
  float radius_;
  char buffer[24];

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "SELECT * FROM POLY WHERE id_ = '");
  _itoa(id_, buffer, 10);
  strcat(sql, buffer);
  strcat(sql, "'");

  sqlite3_prepare_v2(db, sql, 128, &res, &tail);

  if (sqlite3_step(res) == SQLITE_ROW) {
    pos_.x = (float)sqlite3_column_double(res, 1);
    pos_.y = (float)sqlite3_column_double(res, 2);
    radius_ = (float)sqlite3_column_double(res, 3);
  }

  switch ((EntityTag)tag_) {
    case kEntityTag_Enemy: {
      memset(sql, '\0', strlen(sql));
      strcpy(sql, "SELECT * FROM ENEMY WHERE id_ = '");
      _itoa(id_, buffer, 10);
      strcat(sql, buffer);
      strcat(sql, "'");

      sqlite3_prepare_v2(db, sql, 128, &res, &tail);

      sf::Vector2f velocity_;
      float speed_;

      if (sqlite3_step(res) == SQLITE_ROW) {
        speed_ = (float)sqlite3_column_double(res, 1);
        velocity_.x = (float)sqlite3_column_double(res, 2);
        velocity_.y = (float)sqlite3_column_double(res, 3);
      }
      scene->list_[Entity::current_entity_counter_ - 1] = new Enemy((EntityTag)tag_,
                                                              enabled_, {pos_.x, pos_.y},
                                                              6, radius_, 45.0f,
                                                              {1.0f, 1.0f}, 1.0f,
                                                              sf::Color::Black,
                                                              sf::Color::Blue, 1,
                                                              {velocity_.x, velocity_.y},
                                                              speed_);
    }
    break;
    case kEntityTag_Terrain: {
      memset(sql, '\0', strlen(sql));
      strcpy(sql, "SELECT * FROM TERRAIN WHERE id_ = '");
      _itoa(id_, buffer, 10);
      strcat(sql, buffer);
      strcat(sql, "'");

      sqlite3_prepare_v2(db, sql, 128, &res, &tail);

      int terrain_type_;
      sf::Vector2u index;

      if (sqlite3_step(res) == SQLITE_ROW) {
          terrain_type_ = sqlite3_column_int(res, 1);
          index.x = sqlite3_column_int(res, 2);
          index.y = sqlite3_column_int(res, 3);
          terrain_grid[index.x][index.y] = true;
      }

      switch ((TerrainType)terrain_type_) {
        case kTerrainType_Start: {
          scene->list_[Entity::current_entity_counter_ - 1] = new Terrain((EntityTag)tag_,
                                                              enabled_, {pos_.x, pos_.y},
                                                              4, radius_, 45.0f,
                                                              {1.0f, 1.0f}, 1.0f,
                                                              sf::Color::Transparent,
                                                              sf::Color::Green, 1,
                                                              (TerrainType)terrain_type_,
                                                              {index.x, index.y});
        }
        break;
        case kTerrainType_Finish: {
          scene->list_[Entity::current_entity_counter_ - 1] = new Terrain((EntityTag)tag_,
                                                              enabled_, {pos_.x, pos_.y},
                                                              4, radius_, 45.0f,
                                                              {1.0f, 1.0f}, 1.0f,
                                                              sf::Color::Transparent,
                                                              sf::Color::Green, 1,
                                                              (TerrainType)terrain_type_,
                                                              {index.x, index.y});
        }
        break;
        case kTerrainType_Path: {
          scene->list_[Entity::current_entity_counter_ - 1] = new Terrain((EntityTag)tag_,
                                                              enabled_, {pos_.x, pos_.y},
                                                              4, radius_, 45.0f,
                                                              {1.0f, 1.0f}, 1.0f,
                                                              sf::Color::Transparent,
                                                              sf::Color::White, 1,
                                                              (TerrainType)terrain_type_,
                                                              {index.x, index.y});
        }
        break;
        default: {}
        break;
      }
    }
    break;
    case kEntityTag_Player: {
      memset(sql, '\0', strlen(sql));
      strcpy(sql, "SELECT * FROM PLAYER WHERE id_ = '");
      _itoa(id_, buffer, 10);
      strcat(sql, buffer);
      strcat(sql, "'");

      sqlite3_prepare_v2(db, sql, 128, &res, &tail);

      sf::Vector2f velocity_;
      float speed_;
      bool can_move_;

      if(sqlite3_step(res) == SQLITE_ROW){
        velocity_.x = (float)sqlite3_column_double(res, 1);
        velocity_.y = (float)sqlite3_column_double(res, 2);
        speed_ = (float)sqlite3_column_double(res, 3);
        can_move_ = sqlite3_column_int(res, 4);
      }
      scene->list_[Entity::current_entity_counter_ - 1] = new Player((EntityTag)tag_,
                                                          enabled_, {pos_.x, pos_.y},
                                                          4, radius_, 45.0f,
                                                          {1.0f, 1.0f}, 1.0f,
                                                          sf::Color::Black,
                                                          sf::Color::Red, 1,
                                                          {velocity_.x, velocity_.y},
                                                          speed_);
    }
    break;
    default: {}
    break;
  }
}

void GameManager::loadSceneEntities (Scene* scene) {
  sqlite3_stmt *res;
  const char *tail;
  char sql[256];
  char buffer[24];
  int id_;
  int tag_;
  int enabled_;
  bool terrain_grid[kMaxColumns][kMaxColumns];
  for (int i = 0; i < kMaxColumns; ++i) {
    for (int j = 0; j < kMaxColumns; ++j) {
      terrain_grid[i][j] = false;
    }
  }

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "SELECT * FROM ENTITY WHERE scene_id_ = '");
  memset(buffer, '\0', strlen(buffer));
  _itoa(scene->scene_id_, buffer, 10);
  strcat(sql, buffer);
  strcat(sql, "'");

  sqlite3_prepare_v2(db, sql, 128, &res, &tail);

  while (sqlite3_step(res) == SQLITE_ROW) {
    id_ = sqlite3_column_int(res, 0);
    tag_ = sqlite3_column_int(res, 1);
    enabled_ = sqlite3_column_int(res, 2);

    if ((EntityTag)tag_ == kEntityTag_Player ||
        (EntityTag)tag_ == kEntityTag_Terrain ||
        (EntityTag)tag_ == kEntityTag_Enemy) {
      loadPoly(id_, tag_, enabled_, sql, res, tail, scene, terrain_grid);
    }
  }

  //Here we are going to fill the rest of the grid with the missing Terrains
  float tmp_radius = (800.0f * (kSqrt2 / 2) / kMaxColumns);
  for (int i = 0; i < kMaxColumns; ++i) {
    for (int j = 0; j < kMaxColumns; ++j) {
      if (terrain_grid[i][j] == false) {
        scene->list_[Entity::current_entity_counter_ - 1] = new Terrain(kEntityTag_Terrain,
                                                            1, {tmp_radius * kSqrt2 * i +
                                                            tmp_radius * (kSqrt2 / 2),
                                                            tmp_radius * kSqrt2 * j -
                                                            tmp_radius * (kSqrt2 / 2)},
                                                            4,tmp_radius, 45.0f,
                                                            {1.0f, 1.0f}, 0.0f,
                                                            sf::Color::Black,
                                                            sf::Color{179, 179, 255, 255},
                                                            1, kTerrainType_Wall,
                                                            {(unsigned int)i,
                                                            (unsigned int)j});
      }
    }
  }
  sqlite3_finalize(res);
}

void GameManager::writePoly (Poly* poly_, int tag_) {
  char buffer[254];
  char sql[1024];
  char *zErrMsg = 0;
  int rc;

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "INSERT INTO POLY (id_, pos_x, pos_y, radius_) VALUES(");
  memset(buffer, '\0', strlen(buffer));
  _itoa(poly_->id_, buffer, 10);
  strcat(sql, buffer);
  strcat(sql, ",");
  memset(buffer, '\0', strlen(buffer));
  sprintf(buffer, "%f", poly_->pos_.x);
  strcat(sql, buffer);
  strcat(sql, ",");
  memset(buffer, '\0', strlen(buffer));
  sprintf(buffer, "%f", poly_->pos_.y);
  strcat(sql, buffer);
  strcat(sql, ",");
  memset(buffer, '\0', strlen(buffer));
  sprintf(buffer, "%f", poly_->radius_);
  strcat(sql, buffer);
  strcat(sql, ");");

  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

  switch ((EntityTag)tag_) {
    case kEntityTag_Enemy: {
      memset(sql, '\0', strlen(sql));
      strcpy(sql, "INSERT INTO ENEMY (id_, speed_, velocity_x, velocity_y) VALUES(");
      memset(buffer, '\0', strlen(buffer));
      _itoa(((Enemy*)poly_)->id_, buffer, 10);
      strcat(sql, buffer);
      strcat(sql, ",");
      memset(buffer, '\0', strlen(buffer));
      sprintf(buffer, "%f", ((Enemy*)poly_)->speed_);
      strcat(sql, buffer);
      strcat(sql, ",");
      memset(buffer, '\0', strlen(buffer));
      sprintf(buffer, "%f", ((Enemy*)poly_)->velocity_.x);
      strcat(sql, buffer);
      strcat(sql, ",");
      memset(buffer, '\0', strlen(buffer));
      sprintf(buffer, "%f", ((Enemy*)poly_)->velocity_.y);
      strcat(sql, buffer);
      strcat(sql, ");");

      rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    }
    break;
    case kEntityTag_Terrain:{
      if (((Terrain*)poly_)->terrain_type_ != kTerrainType_None &&
          ((Terrain*)poly_)->terrain_type_ != kTerrainType_Wall) {
        memset(sql, '\0', strlen(sql));
        strcpy(sql, "INSERT INTO TERRAIN (id_, terrain_type_, index_x, index_y) VALUES(");
        memset(buffer, '\0', strlen(buffer));
        _itoa(((Terrain*)poly_)->id_, buffer, 10);
        strcat(sql, buffer);
        strcat(sql, ",");
        memset(buffer, '\0', strlen(buffer));
        _itoa(((Terrain*)poly_)->terrain_type_, buffer, 10);
        strcat(sql, buffer);
        strcat(sql, ",");
        memset(buffer, '\0', strlen(buffer));
        _itoa(((Terrain*)poly_)->index_.x, buffer, 10);
        strcat(sql, buffer);
        strcat(sql, ",");
        memset(buffer, '\0', strlen(buffer));
        _itoa(((Terrain*)poly_)->index_.y, buffer, 10);
        strcat(sql, buffer);
        strcat(sql, ");");

        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
      }
    }
    break;
    case kEntityTag_Player: {
      memset(sql, '\0', strlen(sql));
      strcpy(sql, "INSERT INTO PLAYER (id_, velocity_x, velocity_y, speed_, can_move_) VALUES(");
      memset(buffer, '\0', strlen(buffer));
      _itoa(((Player*)poly_)->id_, buffer, 10);
      strcat(sql, buffer);
      strcat(sql, ",");
      memset(buffer, '\0', strlen(buffer));
      sprintf(buffer, "%f", ((Player*)poly_)->velocity_.x);
      strcat(sql, buffer);
      strcat(sql, ",");
      memset(buffer, '\0', strlen(buffer));
      sprintf(buffer, "%f", ((Player*)poly_)->velocity_.y);
      strcat(sql, buffer);
      strcat(sql, ",");
      memset(buffer, '\0', strlen(buffer));
      sprintf(buffer, "%f", ((Player*)poly_)->speed_);
      strcat(sql, buffer);
      strcat(sql, ",");
      memset(buffer, '\0', strlen(buffer));
      _itoa(((Player*)poly_)->can_move_, buffer, 10);
      strcat(sql, buffer);
      strcat(sql, ");");

      rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    }
    break;
    default: {}
    break;
  }
}

void GameManager::writeEntities (Scene* scene) {
  char sql[1024];
  char buffer[254];
  char subquery[254];
  char *zErrMsg = 0;
  int rc;

  memset(subquery, '\0', strlen(subquery));
  memset(buffer, '\0', strlen(buffer));
  strcpy(subquery, "WHERE id_ IN (SELECT id_ FROM ENTITY WHERE scene_id_ = '");
  _itoa(scene->scene_id_, buffer, 10);
  strcat(subquery, buffer);
  strcat(subquery, "');");
  memset(sql, '\0', strlen(sql));
  strcpy(sql, "DELETE FROM TERRAIN ");
  strcat(sql, subquery);

  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "DELETE FROM ENEMY ");
  strcat(sql, subquery);

  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "DELETE FROM PLAYER ");
  strcat(sql, subquery);

  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "DELETE FROM POLY ");
  strcat(sql, subquery);

  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

  memset(sql, '\0', strlen(sql));
  memset(buffer, '\0', strlen(buffer));
  strcpy(sql, "DELETE FROM ENTITY WHERE scene_id_ = '");
  _itoa(scene->scene_id_, buffer, 10);
  strcat(sql, buffer);
  strcat(sql, "'");

  rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

for (int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
  if (scene->list_[i]->tag_ != kEntityTag_Terrain ||
     (scene->list_[i]->tag_ == kEntityTag_Terrain &&
     (((Terrain*)scene->list_[i])->terrain_type_ != kTerrainType_None &&
     ((Terrain*)scene->list_[i])->terrain_type_ != kTerrainType_Wall))) {
     memset(sql, '\0', strlen(sql));
     strcpy(sql, "INSERT INTO ENTITY (id_, tag_, enabled_, scene_id_) VALUES(");
     memset(buffer, '\0', strlen(buffer));
     _itoa(scene->list_[i]->id_, buffer, 10);
     strcat(sql, buffer);
     strcat(sql, ",");
     memset(buffer, '\0', strlen(buffer));
     _itoa(scene->list_[i]->tag_, buffer, 10);
     strcat(sql, buffer);
     strcat(sql, ",");
     memset(buffer, '\0', strlen(buffer));
     _itoa(scene->list_[i]->enabled_, buffer, 10);
     strcat(sql, buffer);
     strcat(sql, ",");
     memset(buffer, '\0', strlen(buffer));
     _itoa(scene->scene_id_, buffer, 10);
     strcat(sql, buffer);
     strcat(sql, ");");

     rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

     if ((EntityTag)scene->list_[i]->tag_ == kEntityTag_Player ||
        (EntityTag)scene->list_[i]->tag_ == kEntityTag_Terrain ||
        (EntityTag)scene->list_[i]->tag_ == kEntityTag_Enemy) {
        writePoly((Poly*)(scene->list_[i]), scene->list_[i]->tag_);
      }
    }
  }
}


int GameManager::readScenes (PlayScene scene_list[kMaxScenes]) {
  sqlite3_stmt *res;
  const char *tail;
  char sql[256];

  int counter = 0;

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "SELECT * FROM SCENE");

  sqlite3_prepare_v2(db, sql, 128, &res, &tail);

  while (sqlite3_step(res) == SQLITE_ROW && counter < kMaxScenes) {
    memset(scene_list[counter].name_, '\0', 256);
    scene_list[counter].scene_id_ = sqlite3_column_int(res, 0);
    strncpy(scene_list[counter].name_, (const char*)sqlite3_column_text(res, 1), 255);
    counter++;
  }

  sqlite3_finalize(res);

  return counter;
}

void GameManager::rearrangeId() {
  sqlite3_stmt *res;
  const char *tail;
  int counter = 0;
  char *zErrMsg = 0;
  int rc = 0;

  sqlite3_prepare_v2(db, "SELECT * FROM ENTITY", 128, &res, &tail);

  while(sqlite3_step(res) == SQLITE_ROW){
    counter++;
  }

  sqlite3_finalize(res);

  int* id_array;
  id_array = (int*)calloc(counter, sizeof(int));

  if(id_array){
    counter = 0;
    sqlite3_prepare_v2(db, "SELECT * FROM ENTITY", 128, &res, &tail);

    while(sqlite3_step(res) == SQLITE_ROW){
      id_array[counter] = sqlite3_column_int(res, 0);
      counter++;
    }

    int len = counter;

  //we order the ids, in case they are not ordered
    while (len > 1) {
      int ordered = 1;
      for (int i = 0; i < (len - 1); i++) {
        if (id_array[i] > id_array[i + 1]) {
          // Swap elements in the array
          int t = id_array[i];
          id_array[i] = id_array[i + 1];
          id_array[i + 1] = t;
          ordered = 0;
        }
      }
      len--;
      if (ordered) {
        len = 0; // nothing more to sort, exit
      }
    }

    char sql[256];
    char buffer[24];
    int tag_;
    int aux_counter = 0;

    memset(sql, '\0', strlen(sql));
    strcpy(sql, "SELECT * FROM ENTITY");

    sqlite3_prepare_v2(db, sql, 128, &res, &tail);

    while (sqlite3_step(res) == SQLITE_ROW) {
      if (id_array[aux_counter] != aux_counter+1) {
        tag_ = sqlite3_column_int(res, 1);

        switch((EntityTag)tag_) {
          case kEntityTag_Player: {
            memset(sql, '\0', strlen(sql));
            strcpy(sql, "UPDATE PLAYER SET id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(aux_counter+1, buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "'");
            strcat(sql, " WHERE id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(id_array[aux_counter], buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "';");

            sqlite3_exec(db, sql, callback, 0, &zErrMsg);
          }
          break;
          case kEntityTag_Terrain: {
            memset(sql, '\0', strlen(sql));
            strcpy(sql, "UPDATE TERRAIN SET id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(aux_counter+1, buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "'");
            strcat(sql, " WHERE id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(id_array[aux_counter], buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "';");

            sqlite3_exec(db, sql, callback, 0, &zErrMsg);
          }
          break;
          case kEntityTag_Enemy: {
            memset(sql, '\0', strlen(sql));
            strcpy(sql, "UPDATE ENEMY SET id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(aux_counter+1, buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "'");
            strcat(sql, " WHERE id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(id_array[aux_counter], buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "';");

            sqlite3_exec(db, sql, callback, 0, &zErrMsg);
          }
          break;
          default: {}
          break;
        }

        if ((EntityTag)tag_ == kEntityTag_Player ||
            (EntityTag)tag_ == kEntityTag_Terrain ||
            (EntityTag)tag_ == kEntityTag_Enemy){
            memset(sql, '\0', strlen(sql));
            strcpy(sql, "UPDATE POLY SET id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(aux_counter+1, buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "'");
            strcat(sql, " WHERE id_ = '");
            memset(buffer, '\0', strlen(buffer));
            _itoa(id_array[aux_counter], buffer, 10);
            strcat(sql, buffer);
            strcat(sql, "';");

            sqlite3_exec(db, sql, callback, 0, &zErrMsg);
        }

        memset(sql, '\0', strlen(sql));
        strcpy(sql, "UPDATE ENTITY SET id_ = '");
        memset(buffer, '\0', strlen(buffer));
        _itoa(aux_counter+1, buffer, 10);
        strcat(sql, buffer);
        strcat(sql, "'");
        strcat(sql, " WHERE id_ = '");
        memset(buffer, '\0', strlen(buffer));
        _itoa(id_array[aux_counter], buffer, 10);
        strcat(sql, buffer);
        strcat(sql, "';");

        sqlite3_exec(db, sql, callback, 0, &zErrMsg);
      }
      aux_counter++;
    }
    sqlite3_finalize(res);
  }

  free(id_array);
  id_array = nullptr;
}


void GameManager::createNewScene (char name[128]) {
  sqlite3_stmt *res;
  const char *tail;
  char sql[256];
  char buffer[24];
  int counter = 0;
  char *zErrMsg = 0;

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "SELECT * FROM SCENE");

  sqlite3_prepare_v2(db, sql, 128, &res, &tail);

  while (sqlite3_step(res) == SQLITE_ROW && counter < kMaxScenes) {
    counter++;
  }

  memset(sql, '\0', strlen(sql));
  strcpy(sql, "INSERT INTO SCENE (scene_id_, scene_name_) VALUES(");
  memset(buffer, '\0', strlen(buffer));
  _itoa(counter+1, buffer, 10);
  strcat(sql, buffer);
  strcat(sql, ", '");
  strcat(sql, name);
  strcat(sql, "');");

  sqlite3_exec(db, sql, callback, 0, &zErrMsg);

  sqlite3_finalize(res);
}


GameManager::GameManager(const GameManager& o) {}

GameManager::~GameManager() {}
