//Author: Jesús González <gonzalezfo@esat-alumni.com>

#include "window.h"

Window& Window::Instance() {
  static Window* inst = nullptr;
  if(inst == nullptr){
    inst = new Window();
  }
  return *inst;
}

Window::Window() {
  window_size_ = {0, 0};
  window_title_ = "Default Window";
  window_.create(sf::VideoMode(window_size_.x, window_size_.y), window_title_);
}

Window::Window (const Window& w) {
  window_size_ = w.window_size_;
  window_title_ = w.window_title_;
  window_.create(sf::VideoMode(window_size_.x, window_size_.y), window_title_);
}

Window::Window (sf::Vector2u newSize, std::string newTitle, sf::RenderWindow& newWindow) {
  window_size_ = newSize;
  window_title_ = newTitle;
  window_.create(sf::VideoMode(window_size_.x, window_size_.y), window_title_);
}

void Window::init() {
  window_size_ = {800, 800};
  window_title_ = "World's Hardest Game";
  window_.create(sf::VideoMode(window_size_.x, window_size_.y), window_title_);
}

void Window::init (sf::Vector2u newSize, std::string newTitle) {
  window_size_ = newSize;
  window_title_ = newTitle;
  window_.create(sf::VideoMode(window_size_.x, window_size_.y), window_title_);
}

void Window::beginDraw (sf::Color newColor) {
  window_.clear(newColor);
}

void Window::endDraw() {
  window_.display();
}

void Window::draw (sf::Drawable& drawable_) {
  window_.draw(drawable_);
}

void Window::destroy() {
  window_.close();
}
