#include "game.h"
#include "game_manager.h"

int main(int argc, char const *argv[]) {

  Game *g = new Game();
  GameManager::Instance().game_ = g;
  g->init();
  g->mainLoop();
  g->finish();

  delete g;
  return 0;
}
