/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include <math.h>
#include "player.h"
#include "input.h"
#include "clock.h"

int Player::player_counter_ = 0;

Player::Player() {
  velocity_ = {0.0f, 0.0f};
  speed_ = 0.0f;
  can_move_ = true;
  ++player_counter_;
}

Player::Player (const Player& o) {
  velocity_ = o.velocity_;
  speed_ = o.speed_;
  can_move_ = o.can_move_;
  ++player_counter_;
}

Player::Player (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
                int newSides, float newRadius, float newRotation,
                sf::Vector2f newScale, float newThickness,
                const sf::Color& newOutlineColor, const sf::Color& newFillColor,
                uint8_t newSolid, sf::Vector2f newVelocity,
                float newSpeed_)  : Poly(newTag, newEnabled, newPos, newSides,
                                         newRadius, newRotation, newScale, newThickness,
                                         newOutlineColor, newFillColor, newSolid) {
  velocity_ = newVelocity;
  speed_ = newSpeed_;
  can_move_ = true;
  ++player_counter_;
}

void Player::init() {
  Poly::init();
  velocity_ = {0.0f, 0.0f};
  speed_ = 0.0f;
  can_move_ = true;
}

void Player::init (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
                   int newSides, float newRadius, float newRotation,
                   sf::Vector2f newScale, float newThickness,
                   const sf::Color& newOutlineColor, const sf::Color& newFillColor,
                   uint8_t newSolid, sf::Vector2f newVelocity, float newSpeed_) {
  Poly::init(newTag, newEnabled, newPos, newSides, newRadius, newRotation,
             newScale, newThickness, newOutlineColor, newFillColor, newSolid);
  velocity_ = newVelocity;
  speed_ = newSpeed_;
}

void Player::move() {
  setPosition(pos_ + velocity_);
}

void Player::update() {
  if (enabled_) {
    float delta_time = 1000.0f * GameClock::Instance().deltaTime(); //asMilisecods doesnt work so we work around it
    velocity_ = {(Input::Instance().right_ - Input::Instance().left_) * delta_time *
                  speed_,(Input::Instance().down_ - Input::Instance().up_) * delta_time * speed_};
    if (can_move_) {
      Player::move();
    }

    can_move_ = true;
    Poly::update();
  }
}

void Player::die() {
  enabled_ = 0;
}

Player::~Player() {
  --player_counter_;
}
