/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "SFML/Graphics/Sprite.hpp"
#include "sprite.h"
#include "window.h"

Sprite::Sprite() {
  pos_ = {0.0f,0.0f};
  height_ = 0.0f;
  width_ = 0.0f;
  scale_ = {1.0f,1.0f};
  rotation_ = 0.0f;
  origin_ = kSpriteOrigin_None;
}

Sprite::Sprite(const Sprite& o) {
  sprite_ = o.sprite_;
  texture_ = o.texture_;
  pos_ = o.pos_;
  height_ = o.height_;
  width_ = o.width_;
  scale_ = o.scale_;
  rotation_ = o.rotation_;
  origin_ = kSpriteOrigin_Copy;
}

Sprite::Sprite (EntityTag newTag, uint8_t newEnabled, sf::Texture newTexture,
                sf::Vector2f newPos, float newHeight,
                float newWidth, sf::Vector2f newScale, float newRotation,
                SpriteOrigin newOrigin) : Entity(newTag, newEnabled) {
  texture_ = newTexture;
  setPosition(newPos);
  height_ = newHeight;
  width_ = newWidth;
  setScale(newScale);
  setRotation(newRotation);
  origin_ = newOrigin;
}

void Sprite::init() {
  pos_ = {0.0f, 0.0f};
  height_ = 0.0f;
  width_ = 0.0f;
  scale_ = {1.0f, 1.0f};
  rotation_ = 0.0f;
  origin_ = kSpriteOrigin_None;
}

void Sprite::init (const char* source) {
  texture_.loadFromFile(source);
  sprite_.setTexture(texture_);
  origin_ = kSpriteOrigin_File;
}

void Sprite::setPosition (sf::Vector2f newPos) {
  pos_ = newPos;
  sprite_.setPosition(pos_);
}

void Sprite::setRotation (float newRot) {
  rotation_ = newRot;
  sprite_.setRotation(rotation_);
}

void Sprite::setScale (sf::Vector2f newScale) {
  scale_ = newScale;
  sprite_.setScale(scale_);
}

float Sprite::getWidth() {
  return width_;
}

float Sprite::getHeight() {
  return height_;
}

sf::Color Sprite::getPixel (sf::Vector2u pixel) {
  sf::Image tmp = texture_.copyToImage();
  return tmp.getPixel(pixel.x, pixel.y);
}

void Sprite::changeOrigin (SpriteOrigin newOrigin) {
  origin_ = newOrigin;
}

void Sprite::update() {
  sprite_.setPosition(pos_);
  sprite_.setRotation(rotation_);
  sprite_.setScale(scale_);
}

void Sprite::draw() {
  if (enabled_) {
    Window::Instance().window_.draw(sprite_);
  }
}

Sprite::~Sprite(){}
