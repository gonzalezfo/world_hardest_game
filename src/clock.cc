//Author: Jesús González <gonzalezfo@esat-alumni.com>

#include "clock.h"

GameClock& GameClock::Instance(){
  static GameClock* inst = nullptr;
  if(inst == nullptr){
    inst = new GameClock();
  }
  return *inst;
}

GameClock::GameClock(){
  delta_time_ = 0.0f;
}

void GameClock::restartClock(){
  delta_time_ = clock_.getElapsedTime().asSeconds();
  clock_.restart();
}

float GameClock::deltaTime(){
  return delta_time_;
}

sf::Time GameClock::elapsedTime(){
  return clock_.getElapsedTime();
}

GameClock::~GameClock(){}
