/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "SFML/System/Vector2.hpp"
#include "SFML/Graphics/Color.hpp"
#include "SFML/Graphics/RectangleShape.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "rect.h"
#include "window.h"

Rect::Rect() {
  pos_ = {0.0f, 0.0f};
  height_ = 0.0f;
  width_ = 0.0f;
  rotation_ = 0.0f;
  scale_ = {1.0f, 1.0f};
  border_thickness_ = 1.0f;
  outline_color_ = sf::Color::White;
  fill_color_ = sf::Color::White;
  solid_ = 0;
}

Rect::Rect (const Rect& o) {
  pos_ = o.pos_;
  height_ = o.height_;
  width_ = o.height_;
  rotation_ = o.rotation_;
  scale_ = o.scale_;
  border_thickness_ = o.border_thickness_;
  outline_color_ = o.outline_color_;
  fill_color_ = o.fill_color_;
  solid_ = o.solid_;
}

Rect::Rect (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
            float newWidth, float newHeight, float newRotation,
            sf::Vector2f newScale, float newThickness,
            const sf::Color& newOutlineColor, const sf::Color& newFillColor,
            uint8_t newSolid) : Entity(newTag, newEnabled) {
  pos_ = newPos;
  height_ = newHeight;
  width_ = newWidth;
  rotation_ = newRotation;
  scale_ = newScale;
  border_thickness_ = newThickness;
  outline_color_ = sf::Color(newOutlineColor);
  fill_color_ = sf::Color(newFillColor);
  solid_ = newSolid;
}


void Rect::init() {
  Entity::init();
  pos_ = {0.0f, 0.0f};
  height_ = 0.0f;
  width_ = 0.0f;
  rotation_ = 0.0f;
  scale_ = {1.0f, 1.0f};
  border_thickness_ = 0.0f;
  outline_color_ = sf::Color(255, 255, 255, 255);
  fill_color_ = sf::Color(255, 255, 255, 255);
  solid_ = 0;
}


void Rect::init (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
                 float newWidth, float newHeight, float newRotation,
                 sf::Vector2f newScale, float newThickness,
                 const sf::Color& newOutlineColor, const sf::Color& newFillColor,
                 uint8_t newSolid) {
  Entity::init(newTag, newEnabled);
  pos_ = newPos;
  height_ = newHeight;
  width_ = newWidth;
  rotation_ = newRotation;
  scale_ = newScale;
  border_thickness_ = newThickness;
  outline_color_ = sf::Color(newOutlineColor);
  fill_color_ = sf::Color(newFillColor);
  solid_ = newSolid;
}

void Rect::draw() { //We pass the render window by reference
  if (enabled_) {
    sf::RectangleShape tmp_rectangle(sf::Vector2f(width_,height_));
    tmp_rectangle.setPosition(pos_);
    tmp_rectangle.setRotation(rotation_);
    tmp_rectangle.setScale(scale_);
    tmp_rectangle.setOutlineThickness(border_thickness_);

    if (solid_) {
      tmp_rectangle.setFillColor(fill_color_);
    } else {
      tmp_rectangle.setFillColor(sf::Color::Transparent);
    }
    tmp_rectangle.setOutlineColor(outline_color_);

    Window::Instance().window_.draw(tmp_rectangle);
  }
}

Rect::~Rect() {}
