/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "play_scene.h"
#include "imgui.h"
#include "imgui-SFML.h"
#include "player.h"
#include "enemy.h"
#include "terrain.h"
#include "game_manager.h"

#include "SFML/Graphics/Color.hpp"
#include "SFML/System/Vector2.hpp"

PlayScene::PlayScene() {
  new_scene_selected_id_ = -1;
  new_scene_selected_type_ = kSceneType_None;
}

PlayScene::PlayScene (int newId) {
  new_scene_selected_id_ = -1;
  new_scene_selected_type_ = kSceneType_None;
  scene_id_ = newId;
}

void PlayScene::init() {
  GameManager::Instance().loadSceneEntities(this);
}

void PlayScene::mainMenu() {
  if (ImGui::BeginMainMenuBar()) {
    if (ImGui::MenuItem("Edit Mode")) {
      new_scene_selected_id_=scene_id_;
      new_scene_selected_type_=kSceneType_Editor;
    }
    if (ImGui::BeginMenu("Exit")) {
      if (ImGui::MenuItem("Exit level")) {
        new_scene_selected_id_=0;
        new_scene_selected_type_=kSceneType_Select;
      }
      if (ImGui::MenuItem("Exit app")) {
        Window::Instance().destroy();
      }
      ImGui::EndMenu();
    }
    ImGui::SameLine(ImGui::GetWindowWidth()-90);
    ImGui::Text("Deaths: %d" , GameManager::Instance().number_deaths_);
    ImGui::EndMainMenuBar();
  }
}

void PlayScene::update() {
  ImGui::SFML::Update(Window::Instance().window_, GameClock::Instance().elapsedTime());
  mainMenu();

  for (int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
    if (list_[i]->tag_ == kEntityTag_Player) {
      for (int j = 0; j < (int)Entity::current_entity_counter_ ; ++j) {
        if (list_[j]->tag_ == kEntityTag_Enemy) {
          if (((Player*)list_[i])->hbox_.intersecCollision(((Enemy*)list_[j])->hbox_)) {
            new_scene_selected_id_ = scene_id_;
            new_scene_selected_type_ = kSceneType_Play;
            GameManager::Instance().number_deaths_++;
          }
        } else if (list_[j]->tag_ == kEntityTag_Terrain && ((Player*)list_[i])->can_move_) {
          if (((Terrain*)list_[j])->terrain_type_ == kTerrainType_Wall ||
              ((Terrain*)list_[j])->terrain_type_ == kTerrainType_None ||
              ((Terrain*)list_[j])->terrain_type_ == kTerrainType_Finish) {
            ((Player*)list_[i])->update();
            if (((Player*)list_[i])->hbox_.intersecCollision(((Terrain*)list_[j])->hbox_)) {
              if (((Terrain*)list_[j])->terrain_type_ == kTerrainType_Finish) {
                new_scene_selected_id_ = 0;
                new_scene_selected_type_ = kSceneType_Select;
              } else {
                ((Player*)list_[i])->can_move_ = false;
              }
            }
            ((Player*)list_[i])->velocity_ = { - ((Player*)list_[i])->velocity_.x,
                                               - ((Player*)list_[i])->velocity_.y};
            ((Player*)list_[i])->move();
          }
        }
      }
    }
    list_[i]->update();
  }
  if (new_scene_selected_id_ != -1) {
    GameManager::Instance().game_->newScene(new_scene_selected_id_, new_scene_selected_type_);
  }
}

void PlayScene::draw() {
  for (int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
    if (list_[i]->tag_ == kEntityTag_Terrain) {
      list_[i]->draw();
    }
  }
  for(int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
    if (list_[i]->tag_ == kEntityTag_Enemy) {
      list_[i]->draw();
    }
  }
  for (int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
    if (list_[i]->tag_ == kEntityTag_Player) {
      list_[i]->draw();
    }
  }
}

PlayScene::~PlayScene(){}
