/*Jesús González Fonseca*/

#include "SFML/System/Vector2.hpp"
#include "SFML/Graphics/Color.hpp"
#include "SFML/Graphics/CircleShape.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include "poly.h"
#include "window.h"

Poly::Poly() {
  pos_ = {0.0f, 0.0f};
  sides_ =  3;
  radius_ = 1.0f;
  rotation_ = 0.0f;
  scale_ = {1.0f, 1.0f};
  border_thickness_ = 1.0f;
  outline_color_ = sf::Color::White;
  fill_color_ = sf::Color::White;
  solid_ = 0;
}

Poly::Poly (const Poly& o) {
  pos_ = o.pos_;
  sides_ = o.sides_;
  radius_ = o.radius_;
  rotation_ = o.rotation_;
  scale_ = o.scale_;
  border_thickness_ = o.border_thickness_;
  outline_color_ = o.outline_color_;
  fill_color_ = o.fill_color_;
  solid_ = o.solid_;
}

Poly::Poly (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
            int newSides, float newRadius, float newRotation,
            sf::Vector2f newScale, float newThickness,
            const sf::Color& newOutlineColor, const sf::Color& newFillColor,
            uint8_t newSolid) : Entity (newTag, newEnabled) {
  setRadius(newRadius);
  setPointCount(newSides);
  setPosition(newPos);
  setRotation(newRotation);
  setScale(newScale);
  setOutlineThickness(newThickness);
  setOutlineColor(newOutlineColor);
  setFillColor(newFillColor);
  solid_ = newSolid;
}


void Poly::init() {
  Entity::init();
  pos_ = {0.0f, 0.0f};
  sides_= 3;
  radius_ = 0.0f;
  rotation_ = 0.0f;
  scale_ = {1.0f, 1.0f};
  border_thickness_ = 1.0f;
  outline_color_ = sf::Color::White;
  fill_color_ = sf::Color::White;
  solid_= 0;
}


void Poly::init (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
                 int newSides, float newRadius, float newRotation,
                 sf::Vector2f newScale, float newThickness,
                 const sf::Color& newOutlineColor, const sf::Color& newFillColor,
                 uint8_t newSolid) {
  Entity::init(newTag, newEnabled);
  setRadius(newRadius);
  setPointCount(newSides);
  setPosition(newPos);
  setRotation(newRotation);
  setScale(newScale);
  setOutlineThickness(newThickness);
  setOutlineColor(newOutlineColor);
  setFillColor(newFillColor);
  solid_ = newSolid;
}

void Poly::draw() { //We pass the render window by reference
  if (enabled_ != 0) {
    if (solid_) {
      poly_.setFillColor(fill_color_);
    } else {
      poly_.setFillColor(sf::Color::Transparent);
    }
    Window::Instance().window_.draw(poly_);
  }
}

Poly::~Poly(){}

void Poly::setRadius (float newRadius) {
  radius_ = newRadius;
  poly_.setRadius(radius_);
}

void Poly::setPointCount (int newSides) {
  sides_ = newSides;
  poly_.setPointCount(sides_);
}

void Poly::setPosition (sf::Vector2f newPos) {
  pos_ = newPos;
  poly_.setPosition(pos_);
}

void Poly::setRotation (float newRotation) {
  rotation_ = newRotation;
  poly_.setRotation(rotation_);
}

void Poly::setScale (sf::Vector2f newScale) {
  scale_ = newScale;
  poly_.setScale(scale_);
}

void Poly::setOutlineThickness (float newThickness) {
  border_thickness_ = newThickness;
  poly_.setOutlineThickness(border_thickness_);
}

void Poly::setOutlineColor (const sf::Color& newOutlineColor) {
  outline_color_ = newOutlineColor;
  poly_.setOutlineColor(outline_color_);
}

void Poly::setFillColor (const sf::Color& newFillColor) {
  fill_color_ = newFillColor;
  poly_.setFillColor(fill_color_);
}

void Poly::update() {
  poly_.setRadius(radius_);
  poly_.setPointCount(sides_);
  poly_.setPosition(pos_);
  poly_.setRotation(rotation_);
  poly_.setScale(scale_);
  poly_.setOutlineThickness(border_thickness_);
  poly_.setOutlineColor(outline_color_);
  poly_.setFillColor(fill_color_);
  hbox_.update(poly_);
}
