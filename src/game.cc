/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "game.h"
#include "clock.h"
#include "editor_scene.h"
#include "play_scene.h"
#include "select_scene.h"
#include "imgui.h"
#include "imgui-SFML.h"
#include "game_manager.h"
#include <SFML/Graphics/Text.hpp>


Game::Game() {
  Window::Instance().init();
  GameClock::Instance().restartClock();
  Input::Instance().init();
  current_scene_ = new SelectScene();
  current_scene_->init();
}

void Game::init() {
  Window::Instance().init({800, 800}, "World's Hardest Game");
  ImGui::SFML::Init(Window::Instance().window_);
}

void Game::mainLoop() {
  while(Window::Instance().window_.isOpen()) {
    input();
    update();
    draw();
    GameClock::Instance().restartClock();
  }
}

void Game::finish() {
  GameManager::Instance().rearrangeId();
  sqlite3_close(GameManager::Instance().db);
  Window::Instance().destroy();
  ImGui::SFML::Shutdown();
}

void Game::newScene(int scene_id, kSceneType type){
  delete current_scene_;
  switch (type) {
    case kSceneType_Editor: {
      Scene* newEditorScene = new EditorScene(scene_id);
      newEditorScene->init();
      current_scene_ = newEditorScene;
    }
    break;
    case kSceneType_Play: {
      Scene* newPlayScene = new PlayScene(scene_id);
      newPlayScene->init();
      current_scene_ = newPlayScene;
    }
    break;
    case kSceneType_Select: {
      Scene* newSelectScene = new SelectScene(scene_id);
      newSelectScene->init();
      current_scene_ = newSelectScene;
    }
    break;
    default: {}
    break;
  }
}

void Game::input() {
  Input::Instance().cacheInput(); //Cache last frame input

  sf::Event event;
  while (Window::Instance().window_.pollEvent(event)) {
    ImGui::SFML::ProcessEvent(event);
    if (event.type == sf::Event::Closed) {
      Window::Instance().destroy();
    }
    Input::Instance().getInput(event);
  }
}


void Game::update() {
  current_scene_->update();
}

void Game::draw() {
  Window::Instance().beginDraw({135, 135, 135, 255});
  current_scene_->draw();
  ImGui::SFML::Render(Window::Instance().window_);
  Window::Instance().endDraw();
}

Game::~Game() {
  delete current_scene_;
}
