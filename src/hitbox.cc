/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "hitbox.h"
#include "window.h"
#include <math.h>

Hitbox::Hitbox(){
  num_points_ = 0;
  for (int i = 0; i < num_points_; ++i) {
    points_[i].x = 0.0f;
    points_[i].y = 0.0f;
    points_[i].z = 0.0f;
  }
}

Hitbox::Hitbox (const Hitbox& o) {
  num_points_ = o.num_points_;
  for (int i = 0; i < num_points_; ++i) {
    points_[i].x = o.points_[i].x;
    points_[i].y = o.points_[i].y;
    points_[i].z = o.points_[i].z;
  }
}

Hitbox::Hitbox (const sf::Vector3f* newPoints, int newNumberPoints) {
  if (newPoints != nullptr) {
    for (int i = 0; i < newNumberPoints; ++i) {
      points_[i].x = newPoints[i].x;
      points_[i].y = newPoints[i].y;
      points_[i].z = newPoints[i].z;
    }
    num_points_ = newNumberPoints;
  }
}

void Hitbox::init() {
  num_points_ = 0;
  for(int i = 0; i < num_points_; ++i) {
    points_[i].x = 0.0f;
    points_[i].y = 0.0f;
    points_[i].z = 0.0f;
  }
}

void Hitbox::init (const sf::Vector3f* newPoints, int newNumberPoints) {
  if (newPoints != nullptr) {
    for (int i = 0; i < newNumberPoints; ++i) {
      points_[i].x = newPoints[i].x;
      points_[i].y = newPoints[i].y;
      points_[i].z = newPoints[i].z;
    }
    num_points_ = newNumberPoints;
  }
}

Hitbox::~Hitbox(){}

void Hitbox::update (sf::CircleShape& shape) {
  int tmp_number_points = shape.getPointCount();
  if (tmp_number_points <= 30) {
    float tmp_rad = shape.getRadius();
    float tmp_rot = shape.getRotation()* 3.141592f / 180.0f;
    sf::Vector2f aux_offset = {0.0f, tmp_rad * kSqrt2};
    sf::Vector2f tmp_pos = shape.getPosition() + aux_offset;
    sf::Vector2f tmp_scal = shape.getScale();
    num_points_ = tmp_number_points;
    float portions = 2 * 3.141592f / tmp_number_points;
    for (int i = 0; i < num_points_; ++i) {
      points_[i].x = cos((portions) * i - tmp_rot) * tmp_rad + tmp_pos.x;
      points_[i].y = sin((portions) * i - tmp_rot) * tmp_rad + tmp_pos.y;
      points_[i].z = 1.0f;
    }
  }
}


//Auxiliary functions
/*----------------------------------------------------------------------------*/
sf::Vector3f Vec2Coef (sf::Vector2f v1,sf::Vector2f v2) {
  sf::Vector3f res;
  sf::Vector2f perp = {v1.y - v2.y, v2.x - v1.x};
  res.x = perp.x;
  res.y = perp.y;
  res.z = -perp.x * v1.x - perp.y * v1.y;
  return res;
}

float Vec3Len (sf::Vector3f vec) {
  float sol = sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
  return sol;
}
/*----------------------------------------------------------------------------*/


bool Hitbox::intersecCollision (const Hitbox& other_hbox) {
  if (num_points_ <= 30 && other_hbox.num_points_ <= 30) {

    //Calculate implicit coefs
    sf::Vector3f implicit_coef_this_hbox[30];
    for (int i = 0; i < num_points_; ++i) {
      sf::Vector2f aux_points[2] = {{points_[i].x, points_[i].y},
                                    {points_[(i + 1) % num_points_].x,
                                     points_[(i + 1) % num_points_].y}};
      implicit_coef_this_hbox[i] = Vec2Coef(aux_points[0], aux_points[1]);
    }

    //Calculate direction vectors
    sf::Vector3f direction_coefs_other_hbox[30];
    for (int i = 0; i < other_hbox.num_points_; ++i) {
      direction_coefs_other_hbox[i] = other_hbox.points_[(i + 1) % other_hbox.num_points_] -
                                                          other_hbox.points_[i];
    }

    //Detect collision
    for (int i = 0; i < num_points_; ++i) {
      for (int j = 0; j < other_hbox.num_points_; ++j) {
        float denom = implicit_coef_this_hbox[i].x * direction_coefs_other_hbox[j].x +
                      implicit_coef_this_hbox[i].y * direction_coefs_other_hbox[j].y;
        if (denom != 0) {
          float t = - (implicit_coef_this_hbox[i].x * other_hbox.points_[j].x +
                       implicit_coef_this_hbox[i].y * other_hbox.points_[j].y +
                       implicit_coef_this_hbox[i].z) / denom;
          if (t >=0 && t <= 1) {
            sf::Vector3f middle_point = (points_[i] + points_[(i + 1) % num_points_]) * 0.5f;
            sf::Vector3f contact_point;
            contact_point.x = other_hbox.points_[j].x + t * direction_coefs_other_hbox[j].x;
            contact_point.y = other_hbox.points_[j].y + t * direction_coefs_other_hbox[j].y;
            contact_point.z = 1.0f;
            float current_length = Vec3Len(middle_point - contact_point);
            float max_length = Vec3Len(points_[i] - points_[(i + 1) % num_points_]);
            if (current_length < max_length * 0.5f) { //Take square of magnitude and compare to half
              return true;
            }
          }
        }
      }
    }
  }
 return false;
}

void Hitbox::draw() {
  sf::VertexArray lines(sf::LineStrip, num_points_ + 1);
  for (int i = 0; i < num_points_; ++i) {
    lines[i].position = {points_[i].x, points_[i].y};
  }
  lines[num_points_].position = {points_[0].x, points_[0].y};
  Window::Instance().window_.draw(lines);
}
