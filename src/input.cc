/*Jesús González Fonseca*/


#include "input.h"

Input& Input::Instance() {
  static Input* inst=nullptr;
  if (inst == nullptr) {
    inst = new Input();
  }
  return *inst;
}

Input::Input() {
  esc_ = 0;
  left_mouse_ = 0;
  right_mouse_ = 0;
  up_ = 0;
  down_ = 0;
  left_ = 0;
  right_ = 0;

  esc_last_ = 0;
  left_mouse_last_ = 0;
  right_mouse_last_ = 0;
  up_last_ = 0;
  down_last_ = 0;
  left_last_ = 0;
  right_last_ = 0;
}

Input::Input (const Input& o) {
  esc_ = o.esc_;
  left_mouse_ = o.left_mouse_;
  right_mouse_ = o.right_mouse_;
  up_ = o.up_;
  down_ = o.down_;
  left_ = o.left_;
  right_ = o.right_;

  esc_last_ = o.esc_last_;
  left_mouse_last_ = o.left_mouse_last_;
  right_mouse_last_ = o.right_mouse_last_;
  up_last_ = o.up_last_;
  down_last_ = o.down_last_;
  left_last_ = o.left_last_;
  right_last_ = o.right_last_;
}

void Input::init() {
  esc_ = 0;
  left_mouse_ = 0;
  right_mouse_ = 0;
  up_ = 0;
  down_ = 0;
  left_ = 0;
  right_ = 0;

  esc_last_ = 0;
  left_mouse_last_ = 0;
  right_mouse_last_ = 0;
  up_last_ = 0;
  down_last_ = 0;
  left_last_ = 0;
  right_last_ = 0;
}

void Input::cacheInput() {
  esc_last_ = esc_;
  left_mouse_last_ = left_mouse_;
  right_mouse_last_ = right_mouse_;
  up_last_ = up_;
  down_last_ = down_;
  left_last_ = left_;
  right_last_ = right_;
}

void Input::getInput (sf::Event event) {
  if (event.type == sf::Event::KeyPressed) {
    if (event.key.code == sf::Keyboard::Escape) {
      esc_ = 1;
    }
    if (event.key.code == sf::Keyboard::Up) {
      up_ = 1;
    }
    if (event.key.code == sf::Keyboard::Down) {
      down_ = 1;
    }
    if (event.key.code == sf::Keyboard::Left) {
      left_ = 1;
    }
    if (event.key.code == sf::Keyboard::Right) {
      right_ = 1;
    }
  }
  if (event.type == sf::Event::MouseButtonPressed) {
    if (event.mouseButton.button == sf::Mouse::Left) {
      left_mouse_ = 1;
    }
    if (event.mouseButton.button == sf::Mouse::Right) {
      right_mouse_ = 1;
    }
  }
  if (event.type == sf::Event::KeyReleased) {
    if (event.key.code == sf::Keyboard::Escape) {
      esc_ = 0;
    }
    if (event.key.code == sf::Keyboard::Up) {
      up_ = 0;
    }
    if (event.key.code == sf::Keyboard::Down) {
      down_ = 0;
    }
    if (event.key.code == sf::Keyboard::Left) {
      left_ = 0;
    }
    if (event.key.code == sf::Keyboard::Right) {
      right_ = 0;
    }
  }
  if (event.type == sf::Event::MouseButtonReleased) {
    if (event.mouseButton.button == sf::Mouse::Left) {
      left_mouse_ = 0;
    }
    if (event.mouseButton.button == sf::Mouse::Right) {
      right_mouse_ = 0;
    }
  }
}

bool Input::isButtonDown (PeripheralType peripheral, int type) {
  if (peripheral == kPeripheralType_Keyboard) {
    switch (type) {
      case sf::Keyboard::Escape: {
        return (!esc_last_ && esc_);
      }
      break;
      case sf::Keyboard::Up:{
        return (!up_last_ && up_);
      }
      break;
      case sf::Keyboard::Down: {
        return (!down_last_ && down_);
      }
      break;
      case sf::Keyboard::Left: {
        return (!left_last_ && left_);
      }
      break;
      case sf::Keyboard::Right: {
        return (!right_last_ && right_);
      }
      break;
      default: {}
      break;
    }
  } else if (peripheral == kPeripheralType_Mouse) {
    switch (type) {
      case sf::Mouse::Left: {
        return (!left_mouse_last_ && left_mouse_);
      }
      break;
      case sf::Mouse::Right: {
        return (!right_mouse_last_ && right_mouse_);
      }
      break;
      default: {}
      break;
    }
  }
  return false;
}

bool Input::isButtonUp (PeripheralType peripheral, int type){
  if (peripheral == kPeripheralType_Keyboard) {
    switch (type) {
      case sf::Keyboard::Escape: {
        return (esc_last_ && !esc_);
      }
      break;
      case sf::Keyboard::Up: {
        return (up_last_ && !up_);
      }
      break;
      case sf::Keyboard::Down: {
        return (down_last_ && !down_);
      }
      break;
      case sf::Keyboard::Left: {
        return (left_last_ && !left_);
      }
      break;
      case sf::Keyboard::Right: {
        return (right_last_ && !right_);
      }
      break;
      default: {}
      break;
    }
  } else if (peripheral == kPeripheralType_Mouse) {
    switch (type) {
      case sf::Mouse::Left: {
        return (left_mouse_last_ && !left_mouse_);
      }
      break;
      case sf::Mouse::Right: {
        return (right_mouse_last_ && !right_mouse_);
      }
      break;
      default: {}
      break;
    }
  }
  return false;
}

Input::~Input() {}
