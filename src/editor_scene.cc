/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "editor_scene.h"
#include "imgui.h"
#include "imgui-SFML.h"
#include "player.h"
#include "enemy.h"
#include "terrain.h"
#include "game_manager.h"

#include "SFML/Graphics/Color.hpp"
#include "SFML/System/Vector2.hpp"

EditorScene::EditorScene(){
  terrain_type_ = kTerrainType_None;
  new_scene_selected_id_ = -1;
  new_scene_selected_type_ = kSceneType_None;
}

EditorScene::EditorScene(int newId){
  terrain_type_ = kTerrainType_None;
  new_scene_selected_id_ = -1;
  new_scene_selected_type_ = kSceneType_None;
  scene_id_ = newId;
}


void EditorScene::fillEmptyTerrain(){
  int n_columns = 25;
  float tmp_radius = (800.0f * (kSqrt2 / 2) / n_columns);
  for (int i = 0; i < n_columns; ++i) {
    for (int j = 0; j < n_columns; ++j) {
      list_[Entity::current_entity_counter_ - 1] = new Terrain(kEntityTag_Terrain,
                                                   1, {tmp_radius * kSqrt2 * i +
                                                   tmp_radius * (kSqrt2/2),
                                                   tmp_radius * kSqrt2 * j -
                                                   tmp_radius * (kSqrt2 / 2)},
                                                   4, tmp_radius, 45.0f, {1.0f,
                                                   1.0f}, 0.0f, sf::Color::Black,
                                                   sf::Color{179, 179, 255, 255},
                                                   1, kTerrainType_None,
                                                   {(unsigned int)i,
                                                   (unsigned int)j});
    }
  }
}

void EditorScene::fillTerrain(){
  int n_columns = 25;
  float tmp_radius = (800.0f * (kSqrt2 / 2) / n_columns);
  for (int i = 0; i < n_columns; ++i) {
    for (int j = 0; j < n_columns; ++j) {
      list_[Entity::current_entity_counter_ - 1] = new Terrain(kEntityTag_Terrain,
                                                   1, {tmp_radius * kSqrt2 * i +
                                                   tmp_radius * (kSqrt2 / 2),
                                                   tmp_radius * kSqrt2 * j -
                                                   tmp_radius * (kSqrt2 / 2)},
                                                   4, tmp_radius, 45.0f, {1.0f,
                                                   1.0f}, 0.0f, sf::Color::Black,
                                                   sf::Color{179, 179, 255, 255},
                                                   1, kTerrainType_Wall,
                                                   {(unsigned int)i,
                                                   (unsigned int)j});
    }
  }
}

void EditorScene::init(){
  selected_=nullptr;

  fillTerrain();
  terrain_type_ = kTerrainType_None;

  GameManager::Instance().loadSceneEntities(this);
}

void EditorScene::mainMenu() {
  if (ImGui::BeginMainMenuBar()) {
    if (ImGui::MenuItem("Play Mode")) {
      new_scene_selected_id_ = scene_id_;
      new_scene_selected_type_ = kSceneType_Play;
    }
  if (ImGui::BeginMenu("Edit")) {
    if (ImGui::MenuItem("Create new level")) {
      Scene::deleteEntities();
      fillTerrain();
    }
    if (ImGui::MenuItem("Clear level")) {
      Scene::deleteEntities();
      fillEmptyTerrain();
    }
    if (ImGui::MenuItem("Save Entities")) {
      GameManager::Instance().writeEntities(this);
    }
  ImGui::EndMenu();
  }
  if (ImGui::BeginMenu("Exit")) {
    if (ImGui::MenuItem("Exit level")) {
      new_scene_selected_id_ = 0;
      new_scene_selected_type_ = kSceneType_Select;
    }
    if (ImGui::MenuItem("Exit app")) {
      Window::Instance().destroy();
    }
    ImGui::EndMenu();
  }
  ImGui::SameLine(ImGui::GetWindowWidth() - 90);
  ImGui::Text("Deaths: %d", GameManager::Instance().number_deaths_);
  ImGui::EndMainMenuBar();
  }
}

void EditorScene::editionTool() {
  ImGui::Begin("Create");

  if (ImGui::Button("Create Player")) {
    if (Entity::current_entity_counter_ < kMaxEntities) {
      if (Player::player_counter_ < 1) {
        list_[Entity::current_entity_counter_ - 1] = new Player(kEntityTag_Player,
                                                     1,
                                                     (sf::Vector2f)sf::Mouse::getPosition(Window::Instance().window_),
                                                     4, 15.0f, 45.0f, {1.0f, 1.0f},
                                                     1.0f, sf::Color::Black,
                                                     sf::Color::Red, 1, {0.0f,0.0f},
                                                     0.1f);
        selected_ = list_[Entity::current_entity_counter_ - 1];
      }
    }
    terrain_type_ = kTerrainType_None;
  }
  if (ImGui::Button("Create Enemy")) {
    if (Entity::current_entity_counter_ < kMaxEntities) {
      list_[Entity::current_entity_counter_ - 1] = new Enemy(kEntityTag_Enemy, 1,
                                                  (sf::Vector2f)sf::Mouse::getPosition(Window::Instance().window_),
                                                  6, 7.0f, 45.0f, {1.0f,1.0f},
                                                  1.0f, sf::Color::Black,
                                                  sf::Color::Blue, 1, {0.0f,0.0f},
                                                  0.0f);
      selected_ = list_[Entity::current_entity_counter_ - 1];
    }
    terrain_type_ = kTerrainType_None;
  }
  if (ImGui::Button("Create Terrain")) {
    ImGui::OpenPopup("Terrain Types");
  }
  if (ImGui::BeginPopup("Terrain Types")) {
    if (ImGui::Button("Create Wall")) {
      terrain_type_=kTerrainType_Wall;
    }
    if (ImGui::Button("Create Path")) {
      terrain_type_=kTerrainType_Path;
    }
    if (ImGui::Button("Create Start")) {
      terrain_type_=kTerrainType_Start;
    }
    if (ImGui::Button("Create Finish")) {
      terrain_type_=kTerrainType_Finish;
    }
    ImGui::EndPopup();
  }
  if (nullptr != selected_) {
    selected_->setPosition((sf::Vector2f)sf::Mouse::getPosition(Window::Instance().window_));
    if (Input::Instance().isButtonDown(kPeripheralType_Mouse, sf::Mouse::Right)) {
      selected_ = nullptr;
    }
  } else if (terrain_type_ != kTerrainType_None) {
    if (Input::Instance().right_mouse_) {
      for (int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
        if (list_[i]->tag_ == kEntityTag_Terrain) {
          if (sf::Mouse::getPosition(Window::Instance().window_).x >
              ((Terrain*)list_[i])->pos_.x - ((Terrain*)list_[i])->radius_ * (kSqrt2 / 2) &&
              sf::Mouse::getPosition(Window::Instance().window_).x <
              ((Terrain*)list_[i])->pos_.x + ((Terrain*)list_[i])->radius_ * (kSqrt2 / 2) &&
              sf::Mouse::getPosition(Window::Instance().window_).y >
              ((Terrain*)list_[i])->pos_.y + ((Terrain*)list_[i])->radius_ * (kSqrt2 / 2) &&
              sf::Mouse::getPosition(Window::Instance().window_).y <
              ((Terrain*)list_[i])->pos_.y + ((Terrain*)list_[i])->radius_ * (1 + kSqrt2)) {
                ((Terrain*)list_[i])->terrain_type_ = terrain_type_;
          }
        }
      }
    }
  }
  ImGui::End();
}

void EditorScene::update() {
  ImGui::SFML::Update(Window::Instance().window_, GameClock::Instance().elapsedTime());
  GameClock::Instance().delta_time_ = 0.0f;
  mainMenu();
  editionTool();

  for (int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
    list_[i]->update();
  }

  if (new_scene_selected_id_ != -1) {
    GameManager::Instance().game_->newScene(new_scene_selected_id_, new_scene_selected_type_);
  }
}

void EditorScene::draw() {
  for (int i = 0; i < (int)Entity::current_entity_counter_; ++i) {
    list_[i]->draw();
    if (list_[i]->tag_ == kEntityTag_Player) {
      ((Player*)list_[i])->hbox_.draw();
    }
  }
}
