//Author: Jesús González <gonzalezfo@esat-alumni.com>

#include "SFML/System/Time.hpp"
#include <math.h>
#include "enemy.h"
#include "clock.h"

int Enemy::total_enemies_ = 0;

Enemy::Enemy() {
  velocity_ = {0.0f,0.0f};
  speed_ = 0.0f;
  ++Enemy::total_enemies_;
}

Enemy::Enemy (const Enemy& o) {
  velocity_ = o.velocity_;
  speed_ = o.speed_;
  ++Enemy::total_enemies_;
}

Enemy::Enemy (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
              int newSides, float newRadius, float newRotation,
              sf::Vector2f newScale, float newThickness,
              const sf::Color& newOutlineColor, const sf::Color& newFillColor,
              uint8_t newSolid, sf::Vector2f newVelocity,
              float newSpeed_)  : Poly (newTag, newEnabled, newPos, newSides,
                                      newRadius, newRotation, newScale, newThickness,
                                      newOutlineColor, newFillColor, newSolid) {
  velocity_ = newVelocity;
  speed_ = newSpeed_;
  ++Enemy::total_enemies_;
}

void Enemy::init() {
  Poly::init();
  velocity_ = {0.0f, 0.0f};
  speed_ = 0.0f;
}

void Enemy::init (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
                  int newSides, float newRadius, float newRotation,
                  sf::Vector2f newScale, float newThickness,
                  const sf::Color& newOutlineColor, const sf::Color& newFillColor,
                  uint8_t newSolid, sf::Vector2f newVelocity, float newSpeed_) {
  Poly::init (newTag, newEnabled, newPos, newSides, newRadius, newRotation,
              newScale, newThickness, newOutlineColor, newFillColor, newSolid);
  velocity_ = newVelocity;
  speed_ = newSpeed_;
}

void Enemy::move() {
  setPosition(pos_ + velocity_);
  Poly::update();
  hbox_.update(poly_);
}

void Enemy::update() {
  if (enabled_) {
    velocity_ = {0.0f,0.0f};
    Enemy::move();
  }
}

void Enemy::die() {
  enabled_=0;
}

Enemy::~Enemy() {
  --Enemy::total_enemies_;
}
