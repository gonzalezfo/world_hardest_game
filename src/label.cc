//Author: Jesús González <gonzalezfo@esat-alumni.com>


#include "SFML/Graphics/Text.hpp"
#include "label.h"
#include "window.h"

Label::Label() {
  character_size_ = 1;
  string_ = "";
  file_source_ = "";
  outline_color_ = sf::Color::Black;
  fill_color_ = sf::Color::Black;
  thickness_ = 1.0f;
  letter_spacing_factor_ = 1.0f;
  line_spacing_factor_ = 1.0f;
  index_ = 0;
}

Label::Label(const Label& o) {
  text_ = o.text_;
  font_ = o.font_;
  character_size_ = o.character_size_;
  string_ = o.string_;
  file_source_ = o.file_source_;
  outline_color_ = o.outline_color_;
  fill_color_ = o.fill_color_;
  thickness_ = o.thickness_;
  letter_spacing_factor_ = o.letter_spacing_factor_;
  line_spacing_factor_ = o.line_spacing_factor_;
  index_ = o.index_;
}

Label::Label(EntityTag newTag, uint8_t newEnabled, unsigned int newSize, const sf::String& newString,
             const sf::String& newSource, const sf::Color& newOutlineColor,
             const sf::Color& newFillColor, float newThickness,
             float newLetterSpacing, float newLineSpacing) : Entity (newTag, newEnabled) {
  loadFont(newSource);
  setFontSize(newSize);
  setString(newString);
  setOutlineColor(newOutlineColor);
  setFillColor(newFillColor);
  setThickness(newThickness);
  setLetterSpacing(newLetterSpacing);
  setLineSpacing(newLineSpacing);
}


Label::~Label() {}

void Label::init() {
  Entity::init();
  character_size_ = 1;
  string_ = "";
  file_source_ = "";
  outline_color_ = sf::Color::Black;
  fill_color_ = sf::Color::Black;
  thickness_ = 1.0f;
  letter_spacing_factor_ = 1.0f;
  line_spacing_factor_ = 1.0f;
  index_ = 0;
}

void Label::init (EntityTag newTag, uint8_t newEnabled, unsigned int newSize, const sf::String& newString,
                  const sf::String& newSource, const sf::Color& newOutlineColor,
                  const sf::Color& newFillColor, float newThickness,
                  float newLetterSpacing, float newLineSpacing) {
  Entity::init(newTag, newEnabled);
  loadFont(newSource);
  setFontSize(newSize);
  setString(newString);
  setOutlineColor(newOutlineColor);
  setFillColor(newFillColor);
  setThickness(newThickness);
  setLetterSpacing(newLetterSpacing);
  setLineSpacing(newLineSpacing);
}

void Label::loadFont (const sf::String& newSource) {
  file_source_ = newSource;
  font_.loadFromFile(file_source_);
  text_.setFont(font_);
}


void Label::setFontSize (unsigned int newSize) {
  character_size_ = newSize;
  text_.setCharacterSize(character_size_);
}

void Label::setString (const sf::String& newString) {
  string_ = newString;
  text_.setString(string_);
}

void Label::setOutlineColor (const sf::Color& newOutlineColor) {
  outline_color_ = newOutlineColor;
  text_.setOutlineColor(outline_color_);
}

void Label::setFillColor (const sf::Color& newFillColor) {
  fill_color_ = newFillColor;
  text_.setFillColor(fill_color_);
}

void Label::setThickness (float newThickness) {
  thickness_ = newThickness;
  text_.setOutlineThickness(thickness_);
}

void Label::setLetterSpacing (float newLetterSpacing) {
  letter_spacing_factor_ = newLetterSpacing;
  text_.setLetterSpacing(letter_spacing_factor_);
}

void Label::setLineSpacing (float newLineSpacing) {
  line_spacing_factor_ = newLineSpacing;
  text_.setLineSpacing(line_spacing_factor_);
}

sf::Vector2f Label::getCharacterPosition (std::size_t newIndex) {
  index_ = newIndex;
  return text_.findCharacterPos(index_);
}

void Label::update() {
  if (enabled_) {
    font_.loadFromFile(file_source_);
    text_.setFont(font_);
    text_.setCharacterSize(character_size_);
    text_.setString(string_);
    text_.setOutlineColor(outline_color_);
    text_.setFillColor(fill_color_);
    text_.setOutlineThickness(thickness_);
    text_.setLetterSpacing(letter_spacing_factor_);
    text_.setLineSpacing(line_spacing_factor_);
  }
}

void Label::draw() {
  if (enabled_) {
    Window::Instance().window_.draw(text_);
  }
}
