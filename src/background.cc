/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include "background.h"
#include "window.h"

Background::Background() {
  scrolls_vertically_ = 1;
  scrolls_horizontally_ = 1;
  velocity_ = {0.0f, 0.0f};
  pos_ = {0.0f, 0.0f};
  origin_ = kSpriteOrigin_None;
  window_width_ = 0;
  window_height_ = 0;
}

Background::Background(const Background& o) {
  scrolls_vertically_ = o.scrolls_vertically_;
  scrolls_horizontally_ = o.scrolls_horizontally_;
  velocity_ = o.velocity_;
  pos_ = o.pos_;
  sprite_ = o.sprite_;
  window_width_ = o.window_width_;
  window_height_ = o.window_height_;
  origin_ = kSpriteOrigin_Copy;
}

Background::Background(EntityTag newTag, uint8_t newEnabled,
                       uint8_t newVertScroll, uint8_t newHorScroll,
                       sf::Vector2f newVel, sf::Vector2f newPos,
                       sf::Sprite newSprite, sf::Texture newTexture,
                       int newHeight, int newWidth, SpriteOrigin newOrigin)
                           : Entity (newTag, newEnabled) {
  scrolls_vertically_ = newVertScroll;
  scrolls_horizontally_ = newHorScroll;
  velocity_ = newVel;
  pos_ = newPos;
  sprite_ = newSprite;
  texture_ = newTexture;
  window_height_ = newHeight;
  window_width_ = newWidth;
  origin_ = newOrigin;
}

void Background::init() {
  Entity::init();
  scrolls_vertically_ = 1;
  scrolls_horizontally_ = 1;
  velocity_ = {0.0f, 0.0f};
  pos_ = {0.0f, 0.0f};
  origin_ = kSpriteOrigin_None;
  window_width_ = 0;
  window_height_ = 0;
}

void Background::init(const char* file_name, sf::Vector2u window_measures){
  if (file_name != nullptr && window_measures.x > 0 && window_measures.y > 0) {
    texture_.loadFromFile(file_name);
    sprite_.setTexture(texture_);
    origin_ = kSpriteOrigin_File;
    window_width_ = window_measures.x;
    window_height_ = window_measures.y;
  }
}

void Background::update(){
  if(enabled_){
    if (scrolls_vertically_) {
      pos_.y += velocity_.y;
      if (pos_.y > sprite_.getGlobalBounds().height ||
          pos_.y < -sprite_.getGlobalBounds().height) {
        pos_.y = 0.0f;
      }
    }
    if (scrolls_horizontally_) {
      pos_.x += velocity_.x;
      if (pos_.x > sprite_.getGlobalBounds().width ||
          pos_.x < -sprite_.getGlobalBounds().width) {
        pos_.x = 0.0f;
      }
    }
    sprite_.setPosition(pos_);
  }
}

void Background::draw(){
  if (enabled_) {
    sf::Vector2f sprite_measures = {sprite_.getGlobalBounds().width,
                                    sprite_.getGlobalBounds().height};
    sf::Vector2i repetitions = {window_width_ / ((int)sprite_measures.x) + 1,
                                window_height_ / ((int)sprite_measures.y) + 1};
    for(int i = -1; i <= repetitions.x; ++i) {
      for(int j = -1; j <= repetitions.y; ++j) {
        sprite_.setPosition({pos_.x + i * sprite_measures.x,
                             pos_.y + j * sprite_measures.y});
        Window::Instance().window_.draw(sprite_);
      }
    }
    sprite_.setPosition(pos_);
  }
}


Background::~Background(){}
