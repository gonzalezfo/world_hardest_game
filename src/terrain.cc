/*                                                  #       #         ####       ###
 * Author: Marc García Carda "Emgizy"              # #   # #       #           #    #
 * Contact: <marcgarciasv@gmail.com>              #   #   #      #   ###      #
 *          <garciacar@esat-alumni.com>          #       #      #     #      #    #
 *                                              #       #        ###          ###
 */

#include <math.h>
#include "terrain.h"
#include "clock.h"

Terrain::Terrain() {
  terrain_type_  = kTerrainType_None;
}

Terrain::Terrain (const Terrain& o) {
  terrain_type_ = o.terrain_type_;
}

Terrain::Terrain (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
                  int newSides, float newRadius, float newRotation,
                  sf::Vector2f newScale, float newThickness,
                  const sf::Color& newOutlineColor, const sf::Color& newFillColor,
                  uint8_t newSolid, TerrainType newType, sf::Vector2u newIndex)
                        : Poly(newTag, newEnabled, newPos, newSides,
                                newRadius, newRotation, newScale, newThickness,
                                newOutlineColor, newFillColor, newSolid) {
  terrain_type_ = newType;
  index_ = newIndex;
}

void Terrain::init() {
  Poly::init();
  terrain_type_ = kTerrainType_None;
}

void Terrain::init (EntityTag newTag, uint8_t newEnabled, sf::Vector2f newPos,
                    int newSides, float newRadius, float newRotation,
                    sf::Vector2f newScale, float newThickness,
                    const sf::Color& newOutlineColor, const sf::Color& newFillColor,
                    uint8_t newSolid, TerrainType newType, sf::Vector2u newIndex) {
  Poly::init(newTag, newEnabled, newPos, newSides, newRadius, newRotation,
             newScale, newThickness, newOutlineColor, newFillColor, newSolid);
  terrain_type_ = newType;
  index_ = newIndex;
}

void Terrain::update()  {
  switch (terrain_type_) {
    case kTerrainType_None: {
      fill_color_ = sf::Color::Transparent;
    }
    break;
    case kTerrainType_Wall: {
      fill_color_ = sf::Color{179, 179, 255, 255};
    }
    break;
    case kTerrainType_Start: {
      fill_color_ = sf::Color::Green;
    }
    case kTerrainType_Finish: {
      fill_color_ = sf::Color::Green;
    }
    break;
    case kTerrainType_Path: {
      fill_color_ = sf::Color::White;
    }
    break;
    default: {}
    break;
  }
  Poly::update();
}

Terrain::~Terrain() {}
